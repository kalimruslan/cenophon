package ru.cenoboy.cenofon.sqlite;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by plekhanov on 29.07.14.
 */
public class DbOpenHelper extends SQLiteOpenHelper {

    private static final String TAG = "DbOpenHelper";
    private static final String DATABASE_NAME = "db.db";
    private static final int DATABASE_VERSION = 3;

    private static DbOpenHelper mInstance;
    private static SQLiteDatabase db;

    private static final String[] CREATE_SCRIPT = new String[]{
            "CREATE TABLE contacts (phone text, name text)",
            "CREATE TABLE proffits (date datetime, money float, name text)",
            "CREATE TABLE referals (date datetime, money float, name text)",
            "CREATE TABLE seen (id text, date datetime, money float, name text)",
            "CREATE TABLE showtimes (path text, date datetime)",
            "CREATE TABLE callhistory (name text, phone text)"

            /*"INSERT  INTO proffits VALUES ('2014-02-01 12:10:45',3.05,'Привет Андрей')",
            "INSERT  INTO proffits VALUES ('2014-02-01 12:05:04',3.05,'Сауна')",
            "INSERT  INTO proffits VALUES ('2014-02-01 12:00:45',3.05,'Привет Андрей')",
            "INSERT  INTO referals VALUES ('+79063323003',3.05)",
            "INSERT  INTO referals VALUES ('+79063323003',3.05)",
            "INSERT  INTO referals VALUES ('+79063323003',3.05)"*/
    };


    public DbOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    /**
     * Get default instance of the class to keep it a singleton
     *
     * @param context the application context
     */
    public static DbOpenHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DbOpenHelper(context);
        }
        return mInstance;
    }

    /**
     * Returns a writable database instance in order not to open and close many
     * SQLiteDatabase objects simultaneously
     *
     * @return a writable instance to SQLiteDatabase
     */
    public SQLiteDatabase getDb() {
        if ((db == null) || (!db.isOpen())) {
            db = this.getWritableDatabase();
        }

        return db;
    }

    @Override
    public void close() {
        super.close();
        if (db != null) {
            db.close();
            db = null;
        }

        mInstance = null;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("how_work", "DbOpenHelper.onCreate()");
        for (String sql_line : CREATE_SCRIPT) {
            try {
                db.execSQL(sql_line);
            } catch (Exception e) {
                String err = (e.getMessage() == null) ? "Unknown Error" : e.getMessage();
                Log.e(TAG, err);
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion<3) {
            db.execSQL("CREATE TABLE showtimes (path text, date datetime)");
            db.execSQL("CREATE TABLE callhistory (name text, phone text)");
            db.execSQL("CREATE TABLE contacts (name text, phone text)");
        }
    }


    public static String getPath(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return getPath(context, pref.getString("dbstorage", "internal").equals("internal"));
    }

    public static String getPath(Context context, boolean internal) {
        return context.getDatabasePath(DATABASE_NAME).getAbsolutePath();
    }
}

