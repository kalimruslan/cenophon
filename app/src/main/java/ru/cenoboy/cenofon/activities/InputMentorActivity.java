package ru.cenoboy.cenofon.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vk.sdk.VKUIHelper;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.cenoboy.cenofon.Constants;
import ru.cenoboy.cenofon.App;
import ru.cenoboy.cenofon.R;
import ru.cenoboy.cenofon.asynctasks.ExecuteAsyncTask;


public class InputMentorActivity extends ActionBarActivity{

    private static int COMMAND_ACTIVATE = 2;

    private String phoneMentor;
    private String user_id;
    private String user_first_name;
    private Spinner spinnerMentor;
    private String countryMentor = "7";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("how_work", "InputMentorActivity.onCreate()");
        super.onCreate(savedInstanceState);
        SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
        user_id = pref.getString("user_id","");
        user_first_name = pref.getString("first_name", "");

        getSupportActionBar().hide();
        setContentView(R.layout.activity_input_mentor);

        ((TextView)findViewById(R.id.personal)).setText(user_first_name + ", укажи номер\nсвоего пригласителя:");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[] {
                "RU (+7)", "UA (+38)", "BY (+375)", "KZ (+7)"
        });
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerMentor = (Spinner)findViewById(R.id.spinnerMentor);
        spinnerMentor.setAdapter(adapter);
        spinnerMentor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        countryMentor = "7";
                        break;
                    case 1:
                        countryMentor = "38";
                        break;
                    case 2:
                        countryMentor = "375";
                        break;
                    case 3:
                        countryMentor = "7";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        try {
            TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            String countryCode = tm.getSimCountryIso();

            if (countryCode!=null) {
                if (countryCode.toLowerCase().equals("ru")) {
                    spinnerMentor.setSelection(0);
                    countryMentor = "RU";
                }

                if (countryCode.toLowerCase().equals("kz")) {
                    spinnerMentor.setSelection(3);
                    countryMentor = "KZ";
                }

                if (countryCode.toLowerCase().equals("ua")) {
                    spinnerMentor.setSelection(1);
                    countryMentor = "UA";
                }

                if (countryCode.toLowerCase().equals("by")) {
                    spinnerMentor.setSelection(2);
                    countryMentor = "BY";
                }
            }

        } catch (Exception ignored) {

        }

    }

    public void btClick(View view) {

        //Считаем телефон
        String phone_raw = ((EditText)findViewById(R.id.EditTextPhone)).getText()
                .toString().trim();
        //Нормализуем
        phoneMentor = normolize(phone_raw);

        if (phoneMentor.length()<5) {
            Toast.makeText(this,"Телефон указан не верно!", Toast.LENGTH_SHORT).show();
            return;
        }


        ContentValues vals = new ContentValues();
        vals.put("referal", phoneMentor);
        ((App)getApplication()).UpdateUserInfo(vals);

        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair("user_id", user_id));
        params.add(new BasicNameValuePair("phone", phoneMentor));
        params.add(new BasicNameValuePair("country", countryMentor));
        ExecuteAsyncTask execute = new ExecuteAsyncTask(InputMentorActivity.this, "Идет регистрация пользователя",
                Constants.COMMAND_CONFIRM_MENTOR, new ProgressDialog(InputMentorActivity.this), params);
        execute.execute();

    }

    public static String normolize(String phone_raw) {
        String result = "";
        for(int i=0;i<phone_raw.length();i++) {
            char ch = phone_raw.charAt(i);
            if (ch<'0')
                continue;
            if (ch>'9')
                continue;
            result+=ch;
        }
        return result;
    }

    public void workServerResponce(JSONObject jsonObject){
        if (jsonObject == null) {
            Toast.makeText(InputMentorActivity.this,"Ошибка: ",Toast.LENGTH_LONG).show();
            return;
        }

        String mes = "";
        try {
            if (jsonObject.getString("status").equals("error")) {
                mes = jsonObject.getString("error");
                if (mes.equals("wrong_user_id")) {
                    Toast.makeText(InputMentorActivity.this, "Ошибка: нет такого пользователя!", Toast.LENGTH_SHORT).show();
                }
                if (mes.equals("wrong_mentor_phone")) {
                    Toast.makeText(InputMentorActivity.this, "Ошибка: неверный телефон наставника", Toast.LENGTH_SHORT).show();
                }
                if (mes.equals("user_not_found")) {
                    Toast.makeText(InputMentorActivity.this, "Ошибка: пользователь не найден!", Toast.LENGTH_SHORT).show();
                }
                if (mes.equals("wrong_mentor_country")) {
                    Toast.makeText(InputMentorActivity.this, "Ошибка: неверный регион номера наставника", Toast.LENGTH_SHORT).show();
                }
                if (mes.equals("mentor_not_found")) {
                    Toast.makeText(InputMentorActivity.this, "Ошибка: наставник не найден", Toast.LENGTH_SHORT).show();
                }

                ContentValues vals = new ContentValues();
                vals.put("referal_error",mes);
                ((App)getApplication()).UpdateUserInfo(vals);

                return;
            }

        } catch (Exception ex) {
            Toast.makeText(InputMentorActivity.this, "Ошибка: "+mes, Toast.LENGTH_SHORT).show();
            return;
        }

        String confirmMentor = "";
        try {
            if(jsonObject.getString("status").equals("success")) {
                confirmMentor = "confirm";
            }
        } catch (Exception ex) {
            Toast.makeText(InputMentorActivity.this,"Ошибка при разборе ответа",Toast.LENGTH_SHORT).show();
            return;
        }

        //Если дошли до сюда значит все ОК, сохраним токен
        SharedPreferences pref = getSharedPreferences("settings",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("confirm_mentor",confirmMentor);
        editor.putString("mentor_phone",phoneMentor);
        editor.commit();

        //Открываем следующее activity
        Intent intent = new Intent(InputMentorActivity.this, PhoneNumberActivity.class);
        InputMentorActivity.this.startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
    }



}
