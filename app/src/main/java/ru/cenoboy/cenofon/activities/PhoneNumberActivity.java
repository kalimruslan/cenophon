package ru.cenoboy.cenofon.activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.vk.sdk.VKUIHelper;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import ru.cenoboy.cenofon.App;
import ru.cenoboy.cenofon.Constants;
import ru.cenoboy.cenofon.NetworkUtil;
import ru.cenoboy.cenofon.R;
import ru.cenoboy.cenofon.adapters.ContactsAdapter;
import ru.cenoboy.cenofon.adapters.PhoneButtonsAdapter;
import ru.cenoboy.cenofon.asynctasks.ExecuteAsyncTask;
import ru.cenoboy.cenofon.asynctasks.LoadAdvTask;
import ru.cenoboy.cenofon.services.SendGeoService;
import ru.cenoboy.cenofon.services.UpdateService;
import ru.cenoboy.cenofon.sqlite.DbOpenHelper;

public class PhoneNumberActivity extends ActionBarActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private ContactsAdapter adapter;
    private PhoneButtonsAdapter buttons_adapter;
    private ListView listView;
    private TextView textView;
    private ProgressBar progressBar;
    public static TextView tvNoNetwork;
    private ImageButton buttonCall;
    String nameCall;
    public static ImageView soonVideo;
    private String userId = "";
    private String userName;
    private String numberCall;
    private GridView gridview;
    private ProgressDialog progress;
    private GoogleApiClient mGoogleApiClient;
    public static boolean limitSoonVideo = false;
    public static boolean isServiceStarted = false;
    BroadcastReceiver receiver;
    private VideoView vv;
    public TextView balance, price;
    String videoFolder;
    File folder;
    String specFolder;
    int countRun;
    static int videoCounter = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("speed", "start onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_number);

        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        if(!isSDPresent)
        {
            Toast.makeText(this,"Для работы приложения необходима установленная карта памяти!",Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        getPrefSettings();
        // Проверяем, включен ли GPS
        LocationManager manager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        boolean hasGps = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // Если запускается программа 10-ый раз, то проверяем ГПС, и если его нет, то выводим алерт
        if(countRun == 5) {
            if (!hasGps) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PhoneNumberActivity.this);
                builder.setCancelable(false);
                builder.setTitle("Хотите получать больше?");
                builder.setMessage(userName + " , чтобы получать больше денег за просмотры, включите свое местоположение.");
                builder.setPositiveButton("Включить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(i);
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Отмена", null);
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
            countRun = 0; // Обнуляем счетчик запуска программы
        }
        countRun = countRun + 1; //увеличиваем счетчик
        SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("count_run", countRun); // записываем в Shared Preferences
        editor.commit();

        initGoogleApiClient();
        initVvDisplaySize();
        getSupportActionBar().hide();
        initComponents();
        clickActions();
        initBroadcastReceiver();
        Log.d("speed", "finish onCreate()");
    }

    // иниц. гугл апи
    private void initGoogleApiClient(){
        Log.d("speed", "init googlApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }
    // Определяем размеры экрана и videoview
    private void initVvDisplaySize(){
        Log.d("speed", "init VvDisplaySize");
        vv = (VideoView)findViewById(R.id.videoView);
        vv.setOnErrorListener(mOnErrorListener);
        Display display = getWindowManager().getDefaultDisplay();
        int width = 0;
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB_MR2) {
            Point size = new Point();
            display.getSize(size);
            width = size.x;
        } else {
            width = display.getWidth();
        }
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, 9 * width / 16);
        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        vv.setLayoutParams(params);
    }
    // Получаем sharedPreferences
    private void getPrefSettings(){
        SharedPreferences pref = getSharedPreferences("settings",MODE_PRIVATE);
        userId = pref.getString("user_id","");
        userName = pref.getString("first_name", "");
        countRun = pref.getInt("count_run", 0);
    }
    // Определяем компоненты на экране
    private void initComponents(){
        Log.d("speed", "init Components");
        balance = (TextView) findViewById(R.id.balance);
        buttonCall = (ImageButton)findViewById(R.id.buttonCall);
        soonVideo = (ImageView) findViewById(R.id.imageViewSoon);
        //soonVideo.setVisibility(View.VISIBLE);
        textView = (TextView) this.findViewById(R.id.textView);
        tvNoNetwork = (TextView) findViewById(R.id.tv_message);
        tvNoNetwork.setText(userName + ", включи мобильный Интернет для просмотра видеоролика.");
        progressBar = (ProgressBar) findViewById(R.id.pb);
        adapter = new ContactsAdapter(this);
        listView = (ListView)findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setVisibility(View.GONE);

        gridview = (GridView) findViewById(R.id.gridView);
        buttons_adapter = new PhoneButtonsAdapter(this);
        gridview.setAdapter(buttons_adapter);
    }
    // подписываение на различные события
    private void clickActions(){
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor cur = adapter.getCursor();
                cur.moveToPosition(i);
                numberCall = cur.getString(cur.getColumnIndex("phone"));
                nameCall = cur.getString(cur.getColumnIndex("name"));
                //listView.setVisibility(View.GONE);
                textView.setText(numberCall);
                UpdateContextHint();
            }
        });


        gridview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                String digit = buttons_adapter.getDigit(i);
                if (!digit.equals("0"))
                    return true;
                digit = "+";
                return onButtonClick(digit, i);
            }
        });
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String digit = buttons_adapter.getDigit(i);
                onButtonClick(digit, i);
            }
        });

    }
    // иниц. ресивер проверки интернет соединения
    private void initBroadcastReceiver(){
        Log.d("speed", "init broadcastReceivers");
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(!NetworkUtil.getConnectivityStatusString(context)){
                    soonVideo.setVisibility(View.INVISIBLE);
                    tvNoNetwork.setVisibility(View.VISIBLE);
                    vv.setTag(null);
                    vv.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.GONE);
                    return;
                }
                else if (NetworkUtil.getConnectivityStatusString(context)) {
                    try {
                        resumeVideo();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    private void resumeVideo() {
        try {
            videoFolder = Environment.getExternalStorageDirectory() + "/cf_video";
            specFolder = getFilesDir().getAbsolutePath() + "/spec";
            folder = new File(videoFolder);
            progressBar.setVisibility(View.GONE);
            if(!folder.exists() || folder.list().length == 0){
                startGetAvailableVideo();
            } else {
                if (!limitSoonVideo) {
                    soonVideo.setVisibility(View.INVISIBLE);
                    playVideo();
                } else {
                    soonVideo.setVisibility(View.VISIBLE);
                }
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        catch (Exception e){
        }
    }

    //Метод, который запускает получение доступных видосов
    private void startGetAvailableVideo() {
        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair("user_id", userId));
        Log.d("speed", "broadcastReceiver() -> ExecuteAsynctask() -> getAvVideo");
        new ExecuteAsyncTask(PhoneNumberActivity.this, Constants.COMMAND_GET_AV_VIDEO, params).execute();
        vv.setTag(null);
        if (vv.isPlaying())
            vv.stopPlayback();
        vv.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    //Получение обновленного баланса
    public void UpdateBalance() throws JSONException {
        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair("user_id", userId));
        /*
        JSONArray paramsJson = new JSONArray();
        paramsJson.put("user_balance");
        params.add(new BasicNameValuePair("params", paramsJson.toString()));
        */

        new ExecuteAsyncTask(PhoneNumberActivity.this, Constants.COMMAND_GET_USER_BALANCE, params).execute();
    }

    public static float getBalance(Context context) {

        SharedPreferences pref = context.getSharedPreferences("settings", MODE_PRIVATE);
        float res = pref.getFloat("balance",0);
        /*Cursor cur = DbOpenHelper.getInstance(context).getDb().rawQuery("SELECT sum(money) FROM seen",null);
        if (cur.moveToNext())
            res+=cur.getFloat(0);*/
        return res;
    }
    // Видео запускается в первый раз
    private void loadVideoFirstTime() {
        progress = new ProgressDialog(this);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.setTitle("Загрузка видео");
        progress.setMessage("Идет первичная загрузка видеороликов");
        progress.show();
        //loadVideo();
    }
    // Загрука доступеых видео
    private void loadVideo() {
        double Latitude = 0;
        double Longitude = 0;
        if (mLastLocation != null) {
            Latitude = mLastLocation.getLatitude();
            Longitude = mLastLocation.getLongitude();
        } else {
            SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
            Latitude = (double)pref.getFloat("cur_lat",0);
            Longitude = (double)pref.getFloat("cur_lng",0);
        }
        /*
        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair("user_id", userId));
        ExecuteAsyncTask execute = new ExecuteAsyncTask(PhoneNumberActivity.this, Constants.COMMAND_GET_AV_VIDEO, params);
        execute.execute();
        */
    }

    public boolean hasVideo() {
        String videoFolder = Environment.getExternalStorageDirectory()+"/cf_video";
        String specFolder = getFilesDir().getAbsolutePath()+"/spec";

        File fVideo = new File(videoFolder);
        File fSpec = new File(specFolder);
        if (!fVideo.exists()) {
            return false;
        }

        if (fVideo.list().length==0)
            return false;

        if (!fSpec.exists()) {
            return false;
        }

        if (fSpec.list().length==0)
            return false;
        return true;
    }

    // Запуск видео
    public void playVideo() throws JSONException, InterruptedException {

        UpdateBalance(); // Обновление баланса

        vv.setTag(null);

        Random r = new Random();
        int i = r.nextInt(folder.list().length);


        // Если есть хоть одно видео
        if (folder.list().length>1) {
            SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
            int counter = 0;
            while (pref.getInt("pervPlay",-1)==i) {
                i = r.nextInt(folder.list().length);
                counter++;
                if (counter>50) {
                    break;
                }
            }
            SharedPreferences.Editor ed = pref.edit();
            ed.putInt("pervPlay",i);
            ed.commit();
        }

        //Прочитаем спецификацию
        try {
            price = (TextView)findViewById(R.id.price);
            price.setText("За просмотр: 0.0");
            FileInputStream fis = new FileInputStream(specFolder+"/"+folder.list()[i]+"/spec.json");
            InputStreamReader reader = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String receiveString = "";
            StringBuilder stringBuilder = new StringBuilder();

            while ( (receiveString = bufferedReader.readLine()) != null ) {
                stringBuilder.append(receiveString);
            }
            fis.close();
            JSONObject obj = new JSONObject(stringBuilder.toString());

            if (!limitSoonVideo) {
                price.setText("За просмотр: " + String.format("%.2f", obj.getDouble("profit") / 10000));
            } else {
                price.setText("За просмотр: 0,0");}

            try {
                vv.setTag(obj.getString("link"));
            } catch (Exception play_try_two) {
                App.getInstance().trackException(play_try_two);
            }

            startCountDown(obj.getInt("profit_duration"), obj.getString("id"), obj.getDouble("profit")/10000, obj.getString("file_name"), obj.getInt("call_duration"));

        } catch (Exception play_try_one) {
            App.getInstance().trackException(play_try_one);
        }

        String uriPath = videoFolder+"/"+folder.list()[i]+"/video.mp4";
        videoCounter ++;
        Uri uri = Uri.parse(uriPath);

        LinearLayout llVidos = (LinearLayout)findViewById(R.id.llVidos);

        vv.setVisibility(View.VISIBLE);
        vv.requestFocus();
        vv.setVideoURI(uri);
        vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                vv.start();
            }
        });
        vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                //mp.release();

            };
        });

        llVidos.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                VideoView vv = (VideoView) findViewById(R.id.videoView);
                if (vv.getTag() != null && !limitSoonVideo) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(vv.getTag().toString()));
                    startActivity(browserIntent);
                    App.getInstance().trackEvent(getString(R.string.event_category_actions),
                            getString(R.string.event_action_clicks), "TAP");
                }
                return false;
            }
        });

        UpdateBalance();
    }


    private MediaPlayer.OnErrorListener mOnErrorListener = new MediaPlayer.OnErrorListener() {

        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            vv.stopPlayback();
            try {
                Log.d("mp_error", "what: " + what + ", extra: " + extra);
                playVideo();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return true;
        }
    };

    private int getShowTimes(String path) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String curdate = format.format(new Date());
        Cursor cur = DbOpenHelper.getInstance(this).getDb().rawQuery("" +
                "SELECT " +
                "   COUNT(1) " +
                "FROM " +
                "   showtimes " +
                "WHERE " +
                "   path=? AND date=?", new String[]{path, curdate});
        cur.moveToNext();
        return cur.getInt(0);
    }

    private void IncrementShowTimes(String path) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String curdate = format.format(new Date());
        DbOpenHelper.getInstance(this).getDb().execSQL("" +
                "DELETE " +
                "FROM " +
                "   showtimes " +
                "WHERE " +
                "   date <> ?", new String[]{curdate});
        DbOpenHelper.getInstance(this).getDb().execSQL("" +
                "INSERT INTO " +
                "   showtimes " +
                "       (path,date) " +
                "VALUES (?,?)", new String[]{path, curdate});
    }

    public void startServiceUpdate() {
        DbOpenHelper.getInstance(this);
        Intent i = new Intent(this, UpdateService.class);
        i.putExtra("user_id", userId);
        Log.d("how_work", "PhoneNumberActivity.startServiceUpdate() -> start");
        if(isServiceStarted){
            stopService(i); // Если сервис запущен то мы его останавливаем
        }
        startService(i); // и запускаем заново
        isServiceStarted = true;
    }

    /***
     * Вычищает мусор
     */
    public void ClearGarbage() {
        //Удалим то что досталось в наследство
        String oldData = getFilesDir().getAbsolutePath()+"/photos";
        File oldfolder = new File(oldData);
        if (oldfolder.exists()) {
            DeleteRecursive(oldfolder);
        }

        //Пройдемся по каталогам и удалим пустые папки а так же все файлы не являющиеся папками
        String videoFolder = Environment.getExternalStorageDirectory()+"/cf_video";
        String specFolder = getFilesDir().getAbsolutePath()+"/spec";

        File folder = new File(videoFolder);
        File specfolder = new File(specFolder);

        if (!folder.exists())
            return;
        for(String subfolder : folder.list()) {
            File f = new File(videoFolder+"/"+subfolder);
            //Если это не директория грохаем файл
            if (!f.isDirectory()) {
                f.delete();
                continue;
            }

            //Если это директория, проверим не пустая ли она
            if (f.list().length==0) {
                //Пустая, грохнем ее
                f.delete();
                continue;
            }

            //Если нет файла со спецификацией тоже считаем что она не пригодная
            boolean specFind = false;
            File spec = new File(specfolder+"/"+subfolder+"/spec.json");
            specFind = spec.exists();


            if (!specFind) {
                DeleteRecursive(f);
            }
        }

    }

    public static void DeleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                DeleteRecursive(child);

        fileOrDirectory.delete();
    }

    private int countdoun_counter=0;

    private void startCountDown(final int profit_duration, final String id, final double profit, final String name, final int callduration) {

        ((App)getApplication()).cur_id = "";
        ((App)getApplication()).cur_profit=0;
        ((App)getApplication()).durtime = 0;
        ((App)getApplication()).cur_name = "";

        final TextView countdown = (TextView)findViewById(R.id.countdown);

        if (id==null) {
            countdown.setText("");
            buttonCall.setEnabled(true);
        }


        ((App)getApplication()).durtime = callduration;
        ((App)getApplication()).cur_id = id;
        ((App)getApplication()).cur_profit=profit;
        ((App)getApplication()). cur_name = name;

        buttonCall.setEnabled(false);
        countdoun_counter = profit_duration;
        final Handler h = new Handler();
        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                if (countdoun_counter==0) {
                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            countdown.setText("");
                            buttonCall.setEnabled(true);
                            t.cancel();
                        }
                    });

                } else if (limitSoonVideo) {
                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            countdown.setText("");
                            buttonCall.setEnabled(true);
                            vv.stopPlayback();
                            vv.setVisibility(View.INVISIBLE);
                            soonVideo.setVisibility(View.VISIBLE);
                            t.cancel();
                        }
                    });

                } else {
                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            countdown.setText(String.valueOf(countdoun_counter));
                        }
                    });

                    countdoun_counter--;
                }
            }
        },0,1000);
    }

    private boolean onButtonClick(String digit, int i) {
        listView.setVisibility(View.VISIBLE);

        textView.setText(textView.getText().toString() + digit);
        UpdateContextHint();

        return true;
    }

    private void UpdateContextHint() {
        String regexp = prepareFilter(textView.getText().toString());
        adapter.getFilter().filter(regexp);

    }

    private String prepareFilter(String numberText) {

        String result = "";
        for (char symbol : numberText.toCharArray()) {
            try {
                result+=regexpForChar(symbol);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private String regexpForChar(char symbol) throws Exception {

        String dataString = getButtonDataString(symbol);
        if (dataString==null)
            return "";

        JSONArray data = new JSONArray(dataString);
        if (data==null)
            return "";

        JSONArray english = data.getJSONArray(1);
        JSONArray localized = data.getJSONArray(2);


        String result = "[";

        result+=data.getString(0);

        for(int i = 0;i<english.length();i++) {
            result+=","+english.getString(i);
        }

        for(int i = 0;i<localized.length();i++) {
            result+=","+localized.getString(i);
        }

        result+="]{1}";
        return result;

    }

    private String getButtonDataString(char symbol) {
        int stringID = PhoneButtonsAdapter.getStringIDByPos(getPositionByDigit(symbol));
        if (stringID==0)
            return null;
        return getString(stringID);
    }

    private int getPositionByDigit(char digit) {
        switch (digit) {
            case '1':
                return 0;
            case '2':
                return 1;
            case '3':
                return 2;
            case '4':
                return 3;
            case '5':
                return 4;
            case '6':
                return 5;
            case '7':
                return 6;
            case '8':
                return 7;
            case '9':
                return 8;
            case '*':
                return 9;
            case '0':
                return 10;
            case '+':
                return 10;
            case '#':
                return 11;
        }
        return  -1;
    }

    public  void btStatic(View view) {
        Intent i = new Intent(this, StatisticActivity.class);
        startActivity(i);

        //Toast.makeText(this,"Программа работает в тестовом режиме. Скоро коммерческий запуск...",Toast.LENGTH_SHORT).show();
    }

    public  void btDel(View view) {
        String numberString = textView.getText().toString();

        if (numberString.length()==0)
            return;

        numberString = numberString.substring(0, numberString.length()-1);
        textView.setText(numberString);

        if (numberString.trim().equals(""))
            listView.setVisibility(View.GONE);

        UpdateContextHint();
    }

    public void btCall(View view) {
        TextView e = (TextView) findViewById(R.id.textView);
        String numberString = e.getText().toString();
        this.performDial(numberString);
    }

    private void performDial(String numberString) {
        Log.d("how_work", "PhoneNumberActivity.performDial() dial");

        if (!numberString.equals("")) {
            Uri number = Uri.parse("tel:" + numberString);
            Intent dial = new Intent(Intent.ACTION_CALL, number);

            ((App)getApplication()).calltime = (new Date()).getTime();
            //stopService(new Intent(this, UpdateService.class));
            //serviceIsStarted = false;
            VideoView vv = (VideoView)findViewById(R.id.videoView);
            if (vv.isPlaying())
                vv.stopPlayback();
            startActivity(dial);
            App.getInstance().trackEvent(getString(R.string.event_category_actions),
                    getString(R.string.event_action_clicks), "CALL");
            finish();
        }
    }

    public void btOpenContacts(View view) {
        Intent intent = new Intent(PhoneNumberActivity.this, CallLogActivity.class);
        startActivityForResult(intent, 1);
    }

    public void btOpenCallLog(View view) {
        Intent intent = new Intent(PhoneNumberActivity.this, CallLogActivity.class);
        startActivityForResult(intent, 2);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        VKUIHelper.onActivityResult(this, reqCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if(data == null){
            Toast.makeText(this, "Выбранный контакт не содержит телефонного номера", Toast.LENGTH_SHORT).show();
            return;
        }

        if(reqCode == 1){
            String phone = data.getStringExtra("phoneCallLog");
            textView.setText(phone);
        }
    }

    // Распарсивает ответ от сервера на обновление баланса
    public void workServerResponceForBalance(JSONObject jsonObject){
        Log.d("how_work", "workServerResponceForBalance(): " + jsonObject);
        if (jsonObject == null) {
            Toast.makeText(PhoneNumberActivity.this,"Ошибка: ",Toast.LENGTH_LONG).show();
            return;
        }
        SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
        //if (Transmission.checkResult(PhoneNumberActivity.this, e, result, command, params)) {
        try {
            if(jsonObject.getString("status").equals("success")) {
                SharedPreferences.Editor edit = pref.edit();
                edit.putFloat("balance", Float.parseFloat(jsonObject.getString("balance")) / 10000);
                edit.commit();
            }

        } catch (Exception ignored) {
        }

        balance.setText("Ваши доходы: " + String.format("%.2f", getBalance(getApplicationContext())) + "руб.");
    }
    // Распарсивает ответ от сервера на доступное видео
    // Если приходит ошибка, то достигнут лимит на просмотр, иначе запускает задачу на скачивание файлов
    public void workServerResponceForAdvVideo(JSONObject jsonObject){
        Log.d("how_work", "workServerResponceForAdvVideo(): " + jsonObject);
        Log.d("speed", "server responce(getAvVideo)");
        videoFolder = Environment.getExternalStorageDirectory()+"/cf_video";
        specFolder = getFilesDir().getAbsolutePath()+"/spec";
        folder = new File(videoFolder);
        progressBar.setVisibility(View.GONE);

        try {
            //TODO Большие сомнения, что парсинг правильный
            if (jsonObject.getString("status").equals("error")) {
                if (jsonObject.getString("error").equals("wrong_user_id")) {
                    limitSoonVideo = true;
                    Toast.makeText(this, R.string.wrong_user_id, Toast.LENGTH_LONG).show();
                }
                if (jsonObject.getString("error").equals("user_not_found")) {
                    limitSoonVideo = true;
                    Toast.makeText(this, R.string.user_not_found, Toast.LENGTH_LONG).show();
                }
                if (jsonObject.getString("error").equals("disable")) {
                    limitSoonVideo = true;
                    Toast.makeText(this, jsonObject.getJSONObject("message").getString("text"), Toast.LENGTH_LONG).show();
                }
                if (jsonObject.getString("error").equals("limit")) {
                    limitSoonVideo = true;
                    Toast.makeText(this, jsonObject.getJSONObject("message").getString("text"), Toast.LENGTH_LONG).show();
                }
                if(limitSoonVideo){
                    tvNoNetwork.setVisibility(View.INVISIBLE);
                    soonVideo.setVisibility(View.VISIBLE);
                    vv.setTag(null);
                    if (vv.isPlaying())
                        vv.stopPlayback();
                    vv.setVisibility(View.INVISIBLE);
                    try {
                        UpdateBalance();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                Log.d("how_work", "workServerResponceForAdvVideo(): no limit video");
                limitSoonVideo = false;
                soonVideo.setVisibility(View.INVISIBLE);
                try{
                    ClearGarbage();
                    if(!folder.exists() || folder.list().length == 0){
                        loadVideoFirstTime();
                        new LoadAdvTask(PhoneNumberActivity.this, progress, jsonObject).execute();
                        return;
                    }else{
                        playVideo();
                    }

                } catch (Exception ex) {
                    Toast.makeText(PhoneNumberActivity.this, "При загрузке видеоматериалов произошла ошибка!", Toast.LENGTH_SHORT).show();
                    progress.dismiss();
                    finish();
                    return;
                }
            }
        } catch (Exception ignored) {
            App.getInstance().trackException(ignored);
        }
    }

    private void startSendGeoService(){
        Intent i = new Intent(this, SendGeoService.class);
        i.putExtra("user_id", userId);
        startService(i);
    }

    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
        //startSendGeoService();
        getLocationDevice();
        registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        startServiceUpdate();
        App.getInstance().trackScreenView(getString(R.string.screen_phone));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(receiver);
        } catch (Exception e) {
            App.getInstance().trackException(e);
            e.printStackTrace();
        }
    }

    public static Location mLastLocation;

    @Override
    public void onConnected(Bundle bundle) {
        /*Log.d("geo_lock", "PHONE: START");
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        Log.d("geo_lock", "PHONE-mLastLocation: " + mLastLocation);
        if (mLastLocation != null) {
            ContentValues vals = new ContentValues();
            vals.put("location",String.valueOf(mLastLocation.getLatitude())+":"+String.valueOf(mLastLocation.getLongitude()));
            ((App)getApplication()).UpdateUserInfo(vals);

            SharedPreferences pref = getSharedPreferences("settings",MODE_PRIVATE);
            SharedPreferences.Editor ed = pref.edit();
            ed.putFloat("cur_lat",(float)mLastLocation.getLatitude());
            ed.putFloat("cur_lng",(float)mLastLocation.getLongitude());
            ed.commit();
            Log.d("geo_lock", "PHONE: LAT: " + mLastLocation.getLatitude() + " LNG: " + mLastLocation.getLongitude());

            Thread getthread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Geocoder geocoder = new Geocoder(PhoneNumberActivity.this, Locale.getDefault());
                        List<Address> list = geocoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);

                        if (list != null) {
                            if (list.size() > 0) {
                                Address address = list.get(0);
                                ContentValues vals = new ContentValues();
                                vals.put("location_address", address.toString());
                                ((App) getApplication()).UpdateUserInfo(vals);
                            }
                        }
                    }catch (Exception ignored) {};
                }
            });

            getthread.start();

        }
        else{
            Log.d("geo_lock", "PHONE: ELSE");
            SharedPreferences pref = getSharedPreferences("settings",MODE_PRIVATE);
            SharedPreferences.Editor ed = pref.edit();
            ed.putFloat("cur_lat", 0);
            ed.putFloat("cur_lng", 0);
            ed.commit();
        }
        startSendGeoService();
        Log.d("geo_lock", "PHONE: FINISH");*/
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}

    //Get geo-location
    private void getLocationDevice() {
        Log.d("geo_lock", "PHONE: START");
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        Log.d("geo_lock", "PHONE-mLastLocation: " + mLastLocation);
        if (mLastLocation != null) {
            ContentValues vals = new ContentValues();
            vals.put("location",String.valueOf(mLastLocation.getLatitude())+":"+String.valueOf(mLastLocation.getLongitude()));
            ((App)getApplication()).UpdateUserInfo(vals);

            SharedPreferences pref = getSharedPreferences("settings",MODE_PRIVATE);
            SharedPreferences.Editor ed = pref.edit();
            ed.putFloat("cur_lat",(float)mLastLocation.getLatitude());
            ed.putFloat("cur_lng",(float)mLastLocation.getLongitude());
            ed.commit();
            Log.d("geo_lock", "PHONE: LAT: " + mLastLocation.getLatitude() + " LNG: " + mLastLocation.getLongitude());

            Thread getthread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Geocoder geocoder = new Geocoder(PhoneNumberActivity.this, Locale.getDefault());
                        List<Address> list = geocoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);

                        if (list != null) {
                            if (list.size() > 0) {
                                Address address = list.get(0);
                                ContentValues vals = new ContentValues();
                                vals.put("location_address", address.toString());
                                ((App) getApplication()).UpdateUserInfo(vals);
                            }
                        }
                    }catch (Exception ignored) {};
                }
            });

            getthread.start();

        }
        /*else{
            Log.d("geo_lock", "PHONE: ELSE");
            SharedPreferences pref = getSharedPreferences("settings",MODE_PRIVATE);
            SharedPreferences.Editor ed = pref.edit();
            ed.putFloat("cur_lat", 0);
            ed.putFloat("cur_lng", 0);
            ed.commit();
        }*/
        if (!isMyServiceRunning() && mLastLocation != null) {
            Log.d("geo_lock", "PHONE: start GeoService");
            startSendGeoService();
        }
        Log.d("geo_lock", "PHONE: CHECK " + isMyServiceRunning());
        Log.d("geo_lock", "PHONE: FINISH");
    }

    //Check SendGeoService: start/stop. Проверка запущен ли SendGeoService
    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("ru.cenoboy.cenofon.services.SendGeoService".equals(service.service.getClassName())) {
                Log.d("geo_lock", "PHONE: isMyServiceRunning: TRUE");
                return true;
            }
        }
        Log.d("geo_lock", "PHONE: isMyServiceRunning: FINISH");
        return false;
    }
}