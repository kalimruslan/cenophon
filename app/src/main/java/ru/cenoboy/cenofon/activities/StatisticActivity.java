package ru.cenoboy.cenofon.activities;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.vk.sdk.VKUIHelper;

import ru.cenoboy.cenofon.fragments.ProffitFragment;
import ru.cenoboy.cenofon.R;
import ru.cenoboy.cenofon.fragments.ReferalsFragment;
import ru.cenoboy.cenofon.fragments.TabListener;


public class StatisticActivity extends ActionBarActivity  {

    FragmentTabHost mTabHost;
    Button btnFeedBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("how_work", "StatisticActivity.onCreate()");
        setContentView(R.layout.activity_statistic);

        btnFeedBack = (Button) findViewById(R.id.btn_feedback);

        // setup action bar for tabs
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);

        FragmentManager fragmentManager = getSupportFragmentManager();


        ActionBar.Tab tab = actionBar.newTab()
                .setText("Доходы")
                .setTabListener(new TabListener(new ProffitFragment()));
        actionBar.addTab(tab);

        tab = actionBar.newTab()
                .setText("Рефералы")
                .setTabListener(new TabListener(new ReferalsFragment()));

        actionBar.addTab(tab);

        btnFeedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(StatisticActivity.this, FeedBackActivity.class);
                startActivity(i);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.statistic, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
    }



}
