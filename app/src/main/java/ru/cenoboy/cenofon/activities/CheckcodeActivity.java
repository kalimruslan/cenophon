package ru.cenoboy.cenofon.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.vk.sdk.VKUIHelper;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.cenoboy.cenofon.App;
import ru.cenoboy.cenofon.Constants;
import ru.cenoboy.cenofon.R;
import ru.cenoboy.cenofon.asynctasks.ExecuteAsyncTask;


public class CheckcodeActivity extends ActionBarActivity{

    private String phone;
    private String user_id;
    private String country;

    private BroadcastReceiver receiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("how_work", "CheckcodeActivity.onCreate()");
        super.onCreate(savedInstanceState);
        SharedPreferences pref = getSharedPreferences("settings",MODE_PRIVATE);

        user_id = getIntent().getStringExtra("user_id");
        phone = getIntent().getStringExtra("phone");
        country = getIntent().getStringExtra("country");

        getSupportActionBar().hide();
        setContentView(R.layout.activity_check_code);


        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(!intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED"))
                    return;

                if (!waitSMS)
                    return;

                Bundle bundle = intent.getExtras();           //---get the SMS message passed in---
                SmsMessage[] msgs = null;
                String msg_from;
                if (bundle != null){
                    try{
                        Object[] pdus = (Object[]) bundle.get("pdus");
                        msgs = new SmsMessage[pdus.length];
                        for(int i=0; i<msgs.length; i++){
                            msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                            msg_from = msgs[i].getOriginatingAddress();
                            String msgBody = msgs[i].getMessageBody();
                            if (msgBody.contains("Код подтверждения:")) {
                                waitSMS = false;
                                onSMS(msgBody.replace("Код подтверждения:","").trim());
                            }
                        }
                    }catch(Exception e){
                    }
                }
            }
        };

        IntentFilter filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        filter.setPriority(999);

    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    /***
     * Вызывается при приходе SMS
     * @param sms - текст сообщения
     */
    private void onSMS(String sms) {
        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair("user_id", user_id));
        params.add(new BasicNameValuePair("code", sms));
        ExecuteAsyncTask execute = new ExecuteAsyncTask(CheckcodeActivity.this, "Проверка кода",
                Constants.COMMAND_CONFIRM_PHONE, new ProgressDialog(CheckcodeActivity.this), params);
        execute.execute();
    }

    public void btClick(View view) {

        //Считаем код
        String code = ((EditText)findViewById(R.id.EditTextPhone)).getText()
                .toString().trim();

        if (code.length()<1) {
            Toast.makeText(this,"Код указан не верно!", Toast.LENGTH_SHORT).show();
            return;
        }


        onSMS(code);

    }

    private static boolean waitSMS = false;

    public void workServerResponce(JSONObject jsonObject){
        if (jsonObject == null) {
            Toast.makeText(CheckcodeActivity.this,"Ошибка: ",Toast.LENGTH_LONG).show();
            return;
        }

        String mes = "";
        try {
            if (jsonObject.getString("status").equals("error")) {
                mes = jsonObject.getString("error");
                if (mes.equals("wrong_user_id")) {
                    Toast.makeText(CheckcodeActivity.this, "Ошибка: нет такого пользователя!", Toast.LENGTH_SHORT).show();
                }
                if (mes.equals("wrong_code")) {
                    Toast.makeText(CheckcodeActivity.this, "Ошибка: не правильный код!", Toast.LENGTH_SHORT).show();
                }
                if (mes.equals("user_not_found")) {
                    Toast.makeText(CheckcodeActivity.this, "Ошибка: пользователь не найден!", Toast.LENGTH_SHORT).show();
                }

                ContentValues vals = new ContentValues();
                vals.put("referal_error",mes);
                ((App)getApplication()).UpdateUserInfo(vals);
                return;
            }

        } catch (Exception ex) {
            Toast.makeText(CheckcodeActivity.this, "Ошибка: "+mes, Toast.LENGTH_SHORT).show();
            return;
        }

        String confirmPhone = "";
        try {
            if(jsonObject.getString("status").equals("success")) {
                confirmPhone = "confirm";
            }
        } catch (Exception ex) {
            Toast.makeText(CheckcodeActivity.this,"Ошибка при разборе ответа",Toast.LENGTH_SHORT).show();
            return;
        }

        //Если дошли до сюда значит все ОК, сохраним токен
        SharedPreferences pref = getSharedPreferences("settings",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("confirmPhone",confirmPhone);
        editor.commit();

        //Открываем следующее activity
        Intent intent = new Intent(CheckcodeActivity.this, InputMentorActivity.class);
        CheckcodeActivity.this.startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
    }



}
