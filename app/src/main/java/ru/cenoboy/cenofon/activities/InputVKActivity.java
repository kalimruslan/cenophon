package ru.cenoboy.cenofon.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKSdkListener;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.cenoboy.cenofon.Constants;
import ru.cenoboy.cenofon.App;
import ru.cenoboy.cenofon.R;
import ru.cenoboy.cenofon.asynctasks.ExecuteAsyncTask;

/**
 * Created by plekhanov on 27.03.15.
 */
public class InputVKActivity extends ActionBarActivity {

    private ProgressDialog progress;
    String responceCommand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("how_work", "InputVKActivity.onCreate()");
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_input_vk_data);

        progress = new ProgressDialog(this);


        ((ImageButton)findViewById(R.id.imageButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VKSdk.initialize(new VKSdkListener() {
                    @Override
                    public void onCaptchaError(VKError captchaError) {
                        Toast.makeText(InputVKActivity.this, "Ошибка: " + captchaError.errorMessage,
                                Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onTokenExpired(VKAccessToken expiredToken) {
                        VKSdk.authorize("friends", "wall", "offline");
                        Toast.makeText(InputVKActivity.this, "Ошибка: token expired",
                                Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onReceiveNewToken(VKAccessToken newToken) {
                        super.onReceiveNewToken(newToken);

                        progress.setTitle("Подключение к ВКонтакте");
                        progress.setMessage("Чтение идентификационных данных");
                        progress.show();

                        SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("vk_token", newToken.accessToken);
                        editor.commit();

                        VKParameters parameters = new VKParameters();
                        parameters.put("fields", "sex,photo_200_orig,bdate,screen_name");
                        VKApi.users().get(parameters).executeWithListener(new VKRequest.VKRequestListener() {
                            @Override
                            public void onComplete(final VKResponse response) {
                                super.onComplete(response);
                                try {
                                    JSONObject user = response.json.getJSONArray("response").getJSONObject(0);
                                    String id = user.getString("id");
                                    String first_name = user.getString("first_name");
                                    String last_name = user.getString("last_name");
                                    String photo_200_orig = user.getString("photo_200_orig");
                                    String bdate = "1.1.1970";
                                    if (user.has("bdate"))
                                        bdate = user.getString("bdate");
                                    String screen_name = user.getString("screen_name");
                                    String sex = user.getString("sex");

                                    progress.setTitle("Подключение к серверу");
                                    progress.setMessage("Авторизация на сервере ЦеноФон");


                                    SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putString("chanell", "user_" + ((App) getApplication()).getDeviceID());
                                    editor.putString("vk_id", id);
                                    editor.putString("first_name", first_name);
                                    editor.putString("last_name", last_name);
                                    editor.putString("photo_200_orig", photo_200_orig);
                                    editor.putString("screen_name", screen_name);
                                    editor.putString("bdate", bdate);
                                    editor.putString("sex", sex);
                                    editor.commit();


                                    ContentValues vals = new ContentValues();
                                    vals.put("chanell", "user_" + ((App) getApplication()).getDeviceID());
                                    vals.put("vk_id", id);
                                    vals.put("first_name", first_name);
                                    vals.put("last_name", last_name);
                                    vals.put("photo_200_orig", photo_200_orig);
                                    vals.put("screen_name", screen_name);
                                    vals.put("sex", sex);
                                    ((App) getApplication()).UpdateUserInfo(vals);


                                    ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
                                    params.add(new BasicNameValuePair("vk_id", id));
                                    new ExecuteAsyncTask(InputVKActivity.this, Constants.COMMAND_GET_USER_STATUS, params ).execute();

                                } catch (Exception ignored) {
                                    progress.dismiss();
                                    Toast.makeText(InputVKActivity.this, "Ошибка при работе с сервером Вконтакте!", Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                    }

                    @Override
                    public void onAccessDenied(VKError authorizationError) {
                        progress.dismiss();
                        Toast.makeText(InputVKActivity.this, "Ошибка при работе с Вконтакте - доступ закрыт!", Toast.LENGTH_LONG).show();
                    }
                }, "5143829");

                VKSdk.authorize("friends", "wall", "offline");
            }
        });
    }

    public void workServerResponce(JSONObject jsonObject) {
        Log.d("how_work", "InputVKActivity.OnCommandResult()");
        progress.dismiss();
        SharedPreferences pref;

        if (jsonObject == null) {
            Toast.makeText(InputVKActivity.this, "Ошибка: ", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            if (jsonObject.getString("status").equals("success")) {
                if (jsonObject.getString("user_status").equals("need_confirm_phone")) {
                    String user_id = jsonObject.getString("user_id");
                    pref = getSharedPreferences("settings", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("user_id", user_id);
                    editor.commit();

                    responceCommand = "need_confirm_phone";
                    Intent intent = new Intent(InputVKActivity.this, InputDataActivity.class);
                    intent.putExtra("responceCommand", responceCommand);
                    InputVKActivity.this.startActivity(intent);
                    finish();
                    return;
                }

                if (jsonObject.getString("user_status").equals("need_confirm_mentor")) {
                    String user_id = jsonObject.getString("user_id");
                    pref = getSharedPreferences("settings", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("user_id", user_id);
                    editor.commit();

                    responceCommand = "need_confirm_mentor";
                    Intent intent = new Intent(InputVKActivity.this, InputMentorActivity.class);
                    intent.putExtra("responceCommand", responceCommand);
                    InputVKActivity.this.startActivity(intent);
                    finish();
                    return;
                }

                if (jsonObject.getString("user_status").equals("registration_complete")) {
                    String user_id = jsonObject.getString("user_id");
                    pref = getSharedPreferences("settings", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("user_id", user_id);
                    editor.putString("confirm_mentor", "confirm");
                    editor.commit();

                    responceCommand = "registration_complete";
                    Intent intent = new Intent(InputVKActivity.this, PhoneNumberActivity.class);
                    intent.putExtra("responceCommand", responceCommand);
                    InputVKActivity.this.startActivity(intent);
                    finish();
                    return;
                }

            }

            try {
                if (jsonObject.getString("status").equals("error")) {
                    if (jsonObject.getString("error").equals("user_not_found")) {
                        //Открываем следующее activity
                        responceCommand = "user_not_found";
                        Intent intent = new Intent(InputVKActivity.this, InputDataActivity.class);
                        intent.putExtra("responceCommand", responceCommand);
                        InputVKActivity.this.startActivity(intent);
                        finish();
                        return;
                    }

                    if (jsonObject.getString("error").equals("wrong_vk_id")) {
                        Toast.makeText(InputVKActivity.this, "Ошибка: Не найден такой id в сети Vkontakte", Toast.LENGTH_LONG).show();
                        return;
                    }

                    if (jsonObject.getString("error").equals("undefined")) {
                        Toast.makeText(InputVKActivity.this, "Неизвестная ошибка", Toast.LENGTH_LONG).show();
                        return;
                    }


                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(InputVKActivity.this, "Ошибка: сервер ЦеноФон вернул " +
                                "данные не по протоколу", Toast.LENGTH_LONG).show();
                    }
                });

            } catch (final Exception ex) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(InputVKActivity.this, "Ошибка: " + ex.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

        @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
    }

}
