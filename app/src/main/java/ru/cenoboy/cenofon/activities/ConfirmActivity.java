package ru.cenoboy.cenofon.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import ru.cenoboy.cenofon.R;


public class ConfirmActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("how_work", "ConfirmActivity.onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        getSupportActionBar().hide();

    }

    public void btClick(View view) {

        SharedPreferences pref = getSharedPreferences("settings",MODE_PRIVATE);
        SharedPreferences.Editor ed = pref.edit();
        ed.putBoolean("confirm",true);
        ed.commit();


        if (!pref.getString("user_id","").equals("")) {
            if(!pref.getString("confirmMentor", "").equals("")) {
                Intent intent = new Intent(this, PhoneNumberActivity.class);
                this.startActivity(intent);
                finish();
                return;
            }
        }

        Intent intent = new Intent(this, InputVKActivity.class);
        this.startActivity(intent);
        finish();
    }


}
