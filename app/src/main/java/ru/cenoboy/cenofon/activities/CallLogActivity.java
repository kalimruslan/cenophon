package ru.cenoboy.cenofon.activities;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.cenoboy.cenofon.App;
import ru.cenoboy.cenofon.R;
import ru.cenoboy.cenofon.adapters.CalLogAdapter;
import ru.cenoboy.cenofon.dao.Callog;

/**
 * Created by User on 10.12.2015.
 */
public class CallLogActivity extends Activity{
    private ListView mCallLogList;
    private ImageView iconContactSearch;
    SimpleCursorAdapter adapter;
    List<Callog> callLogList = null;
    CalLogAdapter callLogAdapter = null;
    int count = 0;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_log);
        mCallLogList = (ListView) findViewById(R.id.call_log_list);
        iconContactSearch = (ImageView) findViewById(R.id.imageView_contact_search);

        iconContactSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CallLogActivity.this, ContactManagerActivity.class);
                startActivityForResult(i, 1);
            }
        });

        // Populate the contact list
        try {
            populateCallLogList();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        mCallLogList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Callog callog = (Callog)mCallLogList.getItemAtPosition(position);
                String phone = callog.getPhoneNumber();
                Intent intent = new Intent();
                intent.putExtra("phoneCallLog", phone);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }


    /**
     * Populate the contact list based on account currently selected in the account spinner.
     */
    private void populateCallLogList() throws ParseException {
        // Build adapter with contact entries
        final Cursor cursor = getCallLog();
        callLogList = new ArrayList<Callog>();

        int name = cursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        int number = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = cursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = cursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = cursor.getColumnIndex(CallLog.Calls.DURATION);

        while (cursor.moveToNext()) {
            count++;
            Callog callog = new Callog();
            callog.setName(cursor.getString(name));
            callog.setPhoneNumber(cursor.getString(number));
            int callType = cursor.getInt(type);
            callog.setCallType(callType);
            String callDate = cursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            callog.setCallDate(dateFormat(callDayTime));
            callog.setCallDuration(cursor.getInt(duration));
            callLogList.add(callog);

            if(count >= 30){
                break;
            }
        }

        callLogAdapter = new CalLogAdapter(CallLogActivity.this, callLogList);
        mCallLogList.setAdapter(callLogAdapter);
    }


    private Cursor getCallLog()
    {
        // Run query
        Uri uri = CallLog.Calls.CONTENT_URI;
        String[] projection = new String[] {
                CallLog.Calls.CACHED_NAME,
                CallLog.Calls.NUMBER,
                CallLog.Calls.DATE,
                CallLog.Calls.DURATION,
                CallLog.Calls.TYPE
        };

        return managedQuery(uri, projection, null, null, CallLog.Calls.DATE + " DESC");
    }

    private String dateFormat(Date callDate) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); // Set your date format
        String currentData = sdf.format(callDate); // Get Date String according to date format
        return currentData;

/*
        Calendar cal = Calendar.getInstance();
        cal.setTime(callDate);
        String formatedDate = cal.get(Calendar.DATE) + "." + (cal.get(Calendar.MONTH) + 1) + "." + cal.get(Calendar.YEAR);
        return formatedDate;
*/
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if(data == null){
            Toast.makeText(this, "Выбранный контакт не содержит телефонного номера", Toast.LENGTH_SHORT).show();
            return;
        }

        if(reqCode == 1){
            String phone = data.getStringExtra("phoneCallLog");
            Intent intent = new Intent();
            intent.putExtra("phoneCallLog", phone);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("how_work", "CallLogActivity.onResume()-START");
        try {
            updateCallLog();
            Log.d("how_work", "CallLogActivity.onResume()-TRY");
            callLogAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            App.getInstance().trackException(e);
            e.printStackTrace();
            Log.d("how_work", "CallLogActivity.onResume()-CATCH");
        }
    }

    public void updateCallLog() {

        ContentValues newValues = new ContentValues();
        newValues.put(CallLog.Calls.TYPE, CallLog.Calls.INCOMING_TYPE);
        newValues.put(CallLog.Calls.DURATION, 50);
        getContentResolver().update(
                ContentUris.withAppendedId(CallLog.Calls.CONTENT_URI, 0),
                newValues,
                null,
                null);
        Log.d("how_work", "CallLogActivity.updateCallLog()");
    }
}
