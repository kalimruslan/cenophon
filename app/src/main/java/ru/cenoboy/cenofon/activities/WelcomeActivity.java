package ru.cenoboy.cenofon.activities;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.vk.sdk.VKUIHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.cenoboy.cenofon.App;
import ru.cenoboy.cenofon.Constants;
import ru.cenoboy.cenofon.Transmission;
import ru.cenoboy.cenofon.R;
import ru.cenoboy.cenofon.asynctasks.ExecuteAsyncTask;


public class WelcomeActivity extends ActionBarActivity {

    private int appVersionInServer;
    final String UPDATE_URL = "https://play.google.com/store/apps/details?id=ru.cenoboy.cenofon";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("how_work", "WelcomeActivity.onCreate()");
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        //getAppVersion(); // Запускаем проверку версии
        Transmission.uiHandler = new Handler();
        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        if (!isSDPresent) {
            Toast.makeText(this, "Для работы приложения необходима установленная карта памяти!", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
        if (!pref.getString("user_id", "").equals("")) {

            if (!pref.getBoolean("confirm", false)) {
                Intent intent = new Intent(this, ConfirmActivity.class);
                this.startActivity(intent);
                finish();
                return;
            }

            if (!pref.getString("confirm_mentor", "").equals("")) {
                Intent intent = new Intent(this, PhoneNumberActivity.class);
                this.startActivity(intent);
                finish();
                return;
            }
        }

        this.setContentView(R.layout.activity_welcome);

    }

    // Отправляем на сервер запрос на получение версии приложения
    private void getAppVersion(){
        new ExecuteAsyncTask(WelcomeActivity.this, Constants.COMMAND_GET_APP_VERSION).execute();
    }

    public void btClick(View view) {
        Intent intent = new Intent(this, ConfirmActivity.class);
        this.startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
        App.getInstance().trackScreenView(getString(R.string.screen_welcome));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
    }

    public void workServerResponce(JSONObject jsonObject) {
        try {
            appVersionInServer = Integer.parseInt(jsonObject.getString("app_ver"));
            Log.d("app_version", "" + appVersionInServer);
            checkAppVersion();
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
    }

    private void checkAppVersion() {
        int appVersionCode;
        try {
            appVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            if(appVersionCode == appVersionInServer){
                return;
            }

            if(appVersionCode < appVersionInServer){
                AlertDialog.Builder quitDialog = new AlertDialog.Builder(
                        WelcomeActivity.this);
                quitDialog.setCancelable(false);
                quitDialog.setTitle("Необходимо обновить программу!");
                quitDialog.setPositiveButton("Обновить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent playMarketIntent = new Intent(Intent.ACTION_VIEW);
                        playMarketIntent.setData(Uri.parse(UPDATE_URL));
                        startActivity(playMarketIntent);
                        finish();
                    }
                });
                quitDialog.show();
            }
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(WelcomeActivity.this, "Произошла ошибка проверки версии приложения", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
}
