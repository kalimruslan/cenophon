package ru.cenoboy.cenofon.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ru.cenoboy.cenofon.R;

/**
 * Created by User on 26.01.2016.
 */
public class FeedBackActivity extends AppCompatActivity implements View.OnClickListener  {
    private final String developerEmail = "cenofon24@gmail.com";
    private EditText et_content, et_subject;
    private Button btn_send;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);

        //toolbarInitialize(); // init toolbar
        componentsInitialize(); // init all components
    }
    private void componentsInitialize() {
        et_subject = (EditText) findViewById(R.id.et_subject);
        et_content = (EditText) findViewById(R.id.et_content);
        btn_send = (Button) findViewById(R.id.btn_sendLetter);
        btn_send.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        emailIntent.setType("plain/text");
        // Кому
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{developerEmail});
        // Тема письма
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, et_subject.getText().toString() + " (Cenophone)");
        // Текст письма
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, et_content.getText().toString());

        startActivity(Intent.createChooser(emailIntent, "Отправка письма..."));
        finish();
    }
}
