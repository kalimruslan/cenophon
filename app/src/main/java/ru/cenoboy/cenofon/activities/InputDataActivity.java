package ru.cenoboy.cenofon.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vk.sdk.VKUIHelper;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import ru.cenoboy.cenofon.Constants;
import ru.cenoboy.cenofon.App;
import ru.cenoboy.cenofon.R;
import ru.cenoboy.cenofon.asynctasks.ExecuteAsyncTask;


public class InputDataActivity extends ActionBarActivity{

    private ProgressDialog progress;
    private String phone;
    private String user_id, id, last_name, photo_200_orig, bdate, screen_name, sex;
    private String user_first_name;
    private String prevResponceCommand;

    private Spinner spinner;
    private String country = "7";
    SharedPreferences pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("how_work", "InputDataActivity.onCreate()");
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_input_data);
        getPrefSettings();

        prevResponceCommand = getIntent().getStringExtra("responceCommand");
        ((TextView)findViewById(R.id.personal)).setText(user_first_name+", введи номер");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[] {
                "RU (+7)", "UA (+38)", "BY (+375)", "KZ (+7)"
        });
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner = (Spinner)findViewById(R.id.spinner);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        country = "7";
                        break;
                    case 1:
                        country = "38";
                        break;
                    case 2:
                        country = "375";
                        break;
                    case 3:
                        country = "7";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        try {
                TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
                String countryCode = tm.getSimCountryIso();

                if (countryCode!=null) {
                    if (countryCode.toLowerCase().equals("ru")) {
                        spinner.setSelection(0);
                        country = "RU";
                    }

                    if (countryCode.toLowerCase().equals("kz")) {
                        spinner.setSelection(3);
                        country = "KZ";
                    }

                    if (countryCode.toLowerCase().equals("ua")) {
                        spinner.setSelection(1);
                        country = "UA";
                    }

                    if (countryCode.toLowerCase().equals("by")) {
                        spinner.setSelection(2);
                        country = "BY";
                    }
                }

        } catch (Exception ignored) {

        }
    }

    private void getPrefSettings(){
        pref = getSharedPreferences("settings",MODE_PRIVATE);
        id = pref.getString("vk_id", "");
        user_id = pref.getString("user_id","");
        user_first_name = pref.getString("first_name","");
        last_name = pref.getString("last_name", "");
        photo_200_orig = pref.getString("photo_200_orig", "");
        screen_name = pref.getString("screen_name", "");
        bdate = pref.getString("bdate", "");
        sex = pref.getString("sex", "");
        user_id = pref.getString("user_id", "");
    }

    private ArrayList<BasicNameValuePair> fillParams() throws JSONException {
        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();

        params.add(new BasicNameValuePair("vk_id", id));

        JSONArray json = new JSONArray();
        JSONObject vk_data = new JSONObject();
        vk_data.put("id", id);
        vk_data.put("first_name", user_first_name);
        vk_data.put("last_name", last_name);
        vk_data.put("sex", sex);
        vk_data.put("screen_name", screen_name);
        vk_data.put("bdate", bdate);
        vk_data.put("photo_200_orig", photo_200_orig);
        json.put(vk_data);

        params.add(new BasicNameValuePair("vk_data", json.toString()));
        params.add(new BasicNameValuePair("phone", phone));
        params.add(new BasicNameValuePair("country", country));

        return params;
    }

    public void btClick(View view) throws JSONException, UnsupportedEncodingException {

        //Считаем телефон
        String phone_raw = ((EditText)findViewById(R.id.EditTextPhone)).getText()
                .toString().trim();
        //Нормализуем
        phone = normolize(phone_raw);

        if (phone.length()<5) {
            Toast.makeText(this,"Телефон указан не верно!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(prevResponceCommand.equals("user_not_found")) {
            ArrayList <BasicNameValuePair> params = fillParams();
            ExecuteAsyncTask execute = new ExecuteAsyncTask(InputDataActivity.this, Constants.COMMAND_USER_REGISTER, params);
            execute.execute();
        }

        if(prevResponceCommand.equals("need_confirm_phone")){
            ArrayList <BasicNameValuePair> params = fillParams();
            ExecuteAsyncTask execute = new ExecuteAsyncTask(InputDataActivity.this, Constants.COMMAND_USER_REGISTER, params);
            execute.execute();
        }

        if(prevResponceCommand.equals("need_confirm_mentor")){
            Intent i = new Intent(InputDataActivity.this,InputMentorActivity.class);
            i.putExtra("user_id",user_id);
            startActivity(i);
            finish();
        }

    }

    private String normolize(String phone_raw) {
        String result = "";
        for(int i=0;i<phone_raw.length();i++) {
            char ch = phone_raw.charAt(i);
            if (ch<'0')
                continue;
            if (ch>'9')
                continue;
            result+=ch;
        }
        return result;
    }



    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
    }

    public void workServerResponce(JSONObject jsonObject){
        if (jsonObject == null) {
            Toast.makeText(InputDataActivity.this,"Ошибка: ",Toast.LENGTH_LONG).show();
            return;
        }

        try {
            if (jsonObject.getString("status").equals("error")) {
                if (jsonObject.getString("error").equals("wrong_vk_id")) {
                    //Открываем следующее activity
                    Toast.makeText(InputDataActivity.this, R.string.wrong_vk_id, Toast.LENGTH_LONG).show();
                    return;
                }

                if (jsonObject.getString("error").equals("wrong_vk_data")) {
                    Toast.makeText(InputDataActivity.this, R.string.wrong_vk_data, Toast.LENGTH_LONG).show();
                    return;
                }

                if (jsonObject.getString("error").equals("wrong_phone")) {
                    Toast.makeText(InputDataActivity.this, R.string.wrong_phone, Toast.LENGTH_LONG).show();
                    return;
                }

                if (jsonObject.getString("error").equals("wrong_country")) {
                    Toast.makeText(InputDataActivity.this, R.string.wrong_country, Toast.LENGTH_LONG).show();
                    return;
                }

                if (jsonObject.getString("error").equals("fail_add_user")) {
                    Toast.makeText(InputDataActivity.this, R.string.fail_add_user, Toast.LENGTH_LONG).show();
                    return;
                }

            }

            if (jsonObject.getString("status").equals("success")) {
                user_id = jsonObject.getString("user_id");
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("user_id", user_id);
                editor.putString("phone", phone);
                editor.putString("country", country);
                editor.commit();
                Intent i = new Intent(InputDataActivity.this, CheckcodeActivity.class);
                i.putExtra("phone", phone);
                i.putExtra("country", country);
                i.putExtra("user_id", user_id);

                ContentValues vals = new ContentValues();
                vals.put("phone", phone);
                vals.put("country", country);
                vals.put("user_id", user_id);
                ((App) getApplication()).UpdateUserInfo(vals);


                startActivity(i);
                finish();
            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }
}
