package ru.cenoboy.cenofon.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by plekhanov on 05.04.15.
 */
public class MessageActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("how_work", "MessageActivity.onCreate()");
        super.onCreate(savedInstanceState);

        new AlertDialog.Builder(this)
                .setTitle("Внимание")
                .setMessage(getIntent().getStringExtra("alert"))
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MessageActivity.this.finish();
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        MessageActivity.this.finish();
                    }
                })
                .create().show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        int id = getIntent().getIntExtra("id",-1);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(id);
    }
}
