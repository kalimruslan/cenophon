package ru.cenoboy.cenofon.asynctasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import ru.cenoboy.cenofon.services.UpdateService;
import ru.cenoboy.cenofon.activities.PhoneNumberActivity;


/**
 * Created by plekhanov on 19.10.14.
 */
public class LoadAdvTask extends AsyncTask<Void, Void, Void> {
    private ProgressDialog progress;
    private int length = 0;
    private int count = 0;
    private Context context;
    private JSONObject adv_result;

    public LoadAdvTask(Context context,ProgressDialog progress,JSONObject adv_result) throws JSONException {
        this.progress = progress;
        this.context = context;
        this.adv_result = adv_result;
    }

    public LoadAdvTask(Context context, JSONObject jsonResult) {
        this.context = context;
        this.adv_result = jsonResult;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (progress!=null) {
            try {
                JSONObject video;
                String test = adv_result.get("video").toString();
                if (!test.equals("[]")) {
                    video = adv_result.getJSONObject("video");
                } else {
                    video = new JSONObject();
                }
                length = video.length();
            } catch (Exception ignored) {};

        }
    }

    @Override
    protected void onProgressUpdate(Void... params) {
        super.onProgressUpdate(params);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (progress!=null)
            progress.dismiss();

        if (context instanceof PhoneNumberActivity) {
            if (!((PhoneNumberActivity) context).hasVideo()) {
                PhoneNumberActivity.soonVideo.setVisibility(View.VISIBLE);
                Toast.makeText(context, "Пока что реклама не загружена!", Toast.LENGTH_SHORT).show();
                return;
            }

            try {
                Log.d("how_work", "LoadAdvTask.onPOST PLAYVIDEO");
                ((PhoneNumberActivity) context).playVideo();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        else{return;}
    }

    @Override
    protected Void doInBackground(Void... params) {
        Log.d("how_work", "LoadAdvTask.doInBackground()");
        try {
            if (adv_result!=null)
                UpdateService.work(context, adv_result, this);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
