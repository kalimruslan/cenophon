package ru.cenoboy.cenofon.asynctasks;

import android.content.Context;
import android.os.AsyncTask;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import ru.cenoboy.cenofon.Constants;
import ru.cenoboy.cenofon.Transmission;
import ru.cenoboy.cenofon.services.SendGeoService;
import ru.cenoboy.cenofon.services.UpdateService;

/**
 * Created by Руслан on 31.12.2015.
 */
public class ExecuteServiceAsyncTask extends AsyncTask<Void, Void, JSONObject> {
    int command;// Команда что делать.
    String ipAddress, userId;
    Context context;
    ArrayList<BasicNameValuePair> pairs = null;

    public ExecuteServiceAsyncTask(int command, ArrayList<BasicNameValuePair> params){
        this.command = command;
        this.pairs = params;
    }

    public ExecuteServiceAsyncTask(Context context, int command, ArrayList<BasicNameValuePair> params){
        this.context = context;
        this.command = command;
        this.pairs = params;
    }

    public ExecuteServiceAsyncTask(int command, String ipAddress){
        this.command = command;
        this.ipAddress = ipAddress;
    }

    public ExecuteServiceAsyncTask(int command, String ipAddress, String userId){
        this.userId = userId;
        this.command = command;
        this.ipAddress = ipAddress;
    }



    @Override
    protected JSONObject doInBackground(Void... params) {
        switch (command) {
            case Constants.COMMAND_VIDEO_VIEW_ADD: return Transmission.videoViewAdd(0, pairs);
            case Constants.COMMAND_GET_USER_BALANCE:
                try {
                    return Transmission.getUserBalance(0, pairs);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            case Constants.COMMAND_GET_AV_VIDEO: return Transmission.getAvVideo(0, pairs);
            case Constants.COMMAND_SEND_GEO: return Transmission.sendGeo(0, pairs);
            case Constants.COMMAND_GET_SYPEX_JSON: return Transmission.getSypexJson(0, ipAddress);
            case Constants.COMMAND_SEND_WITHOUT_GEO: return Transmission.sendWithoutGeo(0, pairs);

        }
        cancel(true);
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        if(jsonObject != null){
            switch (command){
                case Constants.COMMAND_VIDEO_VIEW_ADD: new UpdateService().parseResponceVideoViewAdd(context, jsonObject); break;
                case Constants.COMMAND_GET_USER_BALANCE: new UpdateService().parseResponceGetUserBalance(context, jsonObject); break;
                case Constants.COMMAND_GET_AV_VIDEO: new UpdateService().parseResponceGetAvVideo(context, jsonObject); break;
                case Constants.COMMAND_SEND_GEO: new SendGeoService().parseResponceSendGeo(jsonObject); break;
                case Constants.COMMAND_GET_SYPEX_JSON:
                    new SendGeoService().parseResponceGetSypex(jsonObject, userId);
                    break;
                case Constants.COMMAND_SEND_WITHOUT_GEO: new SendGeoService().parseResponceSendWithoutGeo(jsonObject); break;
            }
        }
        else{
        }

    }
}
