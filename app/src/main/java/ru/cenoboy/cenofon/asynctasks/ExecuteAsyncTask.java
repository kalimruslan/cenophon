package ru.cenoboy.cenofon.asynctasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ru.cenoboy.cenofon.Constants;
import ru.cenoboy.cenofon.Transmission;
import ru.cenoboy.cenofon.activities.CheckcodeActivity;
import ru.cenoboy.cenofon.activities.InputDataActivity;
import ru.cenoboy.cenofon.activities.InputMentorActivity;
import ru.cenoboy.cenofon.activities.InputVKActivity;
import ru.cenoboy.cenofon.activities.PhoneNumberActivity;
import ru.cenoboy.cenofon.activities.WelcomeActivity;
import ru.cenoboy.cenofon.fragments.ProffitFragment;

/**
 * Created by User on 14.12.2015.
 */
public class ExecuteAsyncTask extends AsyncTask<Void, Void, JSONObject> {
    Context context;
    Fragment fragment;
    String progressText;
    int command; // Команда что делать.
    ProgressDialog dialog;
    ArrayList<BasicNameValuePair> pairs = null;

    public ExecuteAsyncTask(Context context, String progressText, int command, ProgressDialog dialog){
        this.context = context;
        this.progressText = progressText;
        this.command = command;
        this.dialog = dialog;
    }


    public ExecuteAsyncTask(Context context, String progressText, int command, ProgressDialog dialog, ArrayList<BasicNameValuePair> params){
        this.context = context;
        this.progressText = progressText;
        this.command = command;
        this.dialog = dialog;
        this.pairs = params;
    }

    public ExecuteAsyncTask(Context context, int command, ArrayList<BasicNameValuePair> params){
        this.context = context;
        this.command = command;
        this.pairs = params;
    }

    public ExecuteAsyncTask(Fragment fragment, int command, ArrayList<BasicNameValuePair> params){
        this.fragment = fragment;
        this.command = command;
        this.pairs = params;
    }

    public ExecuteAsyncTask(Context context, int command) {
        this.context = context;
        this.command = command;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        /*
        if( command == Constants.COMMAND_USER_REGISTER || command == Constants.COMMAND_GET_USER_BALANCE) {
            dialog.setTitle(progressText);
            dialog.setMessage("Ждем...");
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Отмена", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cancel(true);
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
        */
    }

    @Override
    protected JSONObject doInBackground(Void... params) {
        switch (command) {
            case Constants.COMMAND_GET_USER_STATUS: return Transmission.getUserStatus(0, pairs);
            case Constants.COMMAND_USER_REGISTER:
                try {
                    return Transmission.userRegister(0, pairs);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            case Constants.COMMAND_CONFIRM_PHONE: return Transmission.confirmPhone(0, pairs);
            case Constants.COMMAND_CONFIRM_MENTOR: return Transmission.confirmMentor(0, pairs);
            case Constants.COMMAND_GET_USER_BALANCE:
                try {
                    return Transmission.getUserBalance(0, pairs);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            case Constants.COMMAND_GET_AV_VIDEO: return Transmission.getAvVideo(0, pairs);
            case Constants.COMMAND_WITHDRAW: return Transmission.withDraw(0, pairs);
            case Constants.COMMAND_GET_APP_VERSION: return Transmission.getAppVersion();

        }
        cancel(true);
        dialog.dismiss();
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        if(jsonObject != null){
            switch (command){
                case Constants.COMMAND_GET_USER_STATUS: { if (context instanceof InputVKActivity) { ((InputVKActivity) context).workServerResponce(jsonObject);} break;}
                case Constants.COMMAND_USER_REGISTER: { if (context instanceof InputDataActivity) { ((InputDataActivity) context).workServerResponce(jsonObject);} break;}
                case Constants.COMMAND_CONFIRM_PHONE: { if (context instanceof CheckcodeActivity) { ((CheckcodeActivity) context).workServerResponce(jsonObject);} break;}
                case Constants.COMMAND_CONFIRM_MENTOR: { if (context instanceof InputMentorActivity) { ((InputMentorActivity) context).workServerResponce(jsonObject);} break;}
                case Constants.COMMAND_GET_USER_BALANCE: { if (context instanceof PhoneNumberActivity) { ((PhoneNumberActivity) context).workServerResponceForBalance(jsonObject);} break;}
                case Constants.COMMAND_GET_AV_VIDEO: {
                    if (context instanceof PhoneNumberActivity) {
                        ((PhoneNumberActivity) context).workServerResponceForAdvVideo(jsonObject);
                    }
                    break;
                }
                case Constants.COMMAND_WITHDRAW: { if (fragment instanceof ProffitFragment) { ((ProffitFragment) fragment).parseResponseWithDraw(jsonObject);} break;}
                case Constants.COMMAND_GET_APP_VERSION: { if (context instanceof WelcomeActivity) { ((WelcomeActivity) context).workServerResponce(jsonObject);} break;}
            }
        }
        /*
        else{
            dialog.dismiss();
        }
        */

    }

}
