package ru.cenoboy.cenofon.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.FilterQueryProvider;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.regex.Pattern;

import ru.cenoboy.cenofon.sqlite.DbOpenHelper;
import ru.cenoboy.cenofon.R;

/**
 * Created by radik on 02.09.14.
 */
public class ContactsAdapter extends CursorAdapter implements FilterQueryProvider {
    private Context context;

    private static int LIMIT = 20;

    private ArrayList<String> items;

    private static final String DISPLAY_NAME =  Build.VERSION.SDK_INT
            >= Build.VERSION_CODES.HONEYCOMB ?
            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY :
            ContactsContract.Contacts.DISPLAY_NAME;

    @SuppressLint("InlinedApi")
    private static final String[] PROJECTION =    {
        ContactsContract.Contacts._ID,
        ContactsContract.Contacts.LOOKUP_KEY,
        DISPLAY_NAME

    };

    @SuppressLint("InlinedApi")
    private static final String[] PROJECTION_PHONE = {
        ContactsContract.CommonDataKinds.Phone._ID,
        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
        ContactsContract.CommonDataKinds.Phone.NUMBER
    };

    public ContactsAdapter(Context context) {
        super(context, null);
        this.context = context;
        setFilterQueryProvider(this);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.cotact_item, null);
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView name = (TextView)view.findViewById(R.id.name);
        if (name!=null) {
            name.setText(cursor.getString(cursor.getColumnIndex("name")));
        }

        TextView label = (TextView)view.findViewById(R.id.label);
        if (label!=null) {
            label.setText(cursor.getString(cursor.getColumnIndex("phone")));
        }

    }

    private String getLabel(Cursor cursor) {

        int idx = cursor.getColumnIndex(ContactsContract.Contacts._ID);
        String contactID = cursor.getString(idx);

        Cursor cursorPhone = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                new String[]{contactID},
                null);

        String result = "Ничего нету";
        if (cursorPhone.getCount()==1){
            cursorPhone.moveToFirst();
            result = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        }
        cursorPhone.close();

        return result;

    }


    @Override
    public Cursor runQuery(CharSequence filter) {
        Log.d("how_work", "ContactsAdapter.runQuery()");

        Pattern p = Pattern.compile(filter.toString(),Pattern.CASE_INSENSITIVE);


        Cursor cur = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                         PROJECTION_PHONE,null, null, null);

        SQLiteDatabase db = DbOpenHelper.getInstance(context).getDb();

        int i =0;

        db.beginTransaction();
        try {
            db.execSQL("DELETE from contacts");

            while (cur.moveToNext()) {

                String number = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                if (match(number, name, p)) {
                    i++;
                    db.execSQL("INSERT INTO contacts VALUES (?,?)", new String[]{number, name});
                    if (i>LIMIT)
                        break;
                }
            }
            cur.close();
            db.setTransactionSuccessful();
        } catch (Exception e) {
            ;
        } finally {
            db.endTransaction();
        }

        return db.rawQuery("SELECT phone as '_id', * FROM contacts LIMIT ? ",new String[] {String.valueOf(LIMIT)});

    }


    private boolean match(String number, String name, Pattern p) {

        if (p.matcher(number).find() || p.matcher(name).find())
            return true;
        return false;
    }

}
