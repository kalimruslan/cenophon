package ru.cenoboy.cenofon.adapters;

import android.content.Context;
import android.provider.CallLog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ru.cenoboy.cenofon.App;
import ru.cenoboy.cenofon.R;
import ru.cenoboy.cenofon.dao.Callog;

/**
 * Created by User on 10.12.2015.
 */
public class CalLogAdapter extends ArrayAdapter<Callog> {
    Context context;
    List<Callog> callogList;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        try {
            Callog callog = callogList.get(position);
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.call_log_entry, parent,false);

            TextView txtName = (TextView)view.findViewById(R.id.txt_call_log_name);
            TextView txtNumber = (TextView)view.findViewById(R.id.txt_call_log_number);
            TextView txtDate = (TextView)view.findViewById(R.id.txt_call_log_date);
            TextView txtCallDuration = (TextView)view.findViewById(R.id.txt_call_log_duration);
            TextView txtCallType = (TextView)view.findViewById(R.id.txt_call_log_type);
            ImageView imgCallType = (ImageView)view.findViewById(R.id.img_call_type);
            ImageView imgCallCheck = (ImageView)view.findViewById(R.id.img_call_check);


            txtName.setText(callog.getName());
            //txtCallType.setText(callog.getCallType());
            try {
                if (callog.getCallType() == CallLog.Calls.OUTGOING_TYPE)
                    imgCallType.setImageResource(R.drawable.ic_call_out);
                if (callog.getCallType() == CallLog.Calls.INCOMING_TYPE)
                    imgCallType.setImageResource(R.drawable.ic_call_in);
                if (callog.getCallType() == CallLog.Calls.MISSED_TYPE)
                    imgCallType.setImageResource(R.drawable.ic_call_missed);
            }
            catch (Exception setTypeCall){
                App.getInstance().trackException(setTypeCall);
                setTypeCall.printStackTrace();
            }

            txtNumber.setText(callog.getPhoneNumber());
            txtDate.setText(callog.getCallDate());

            int minutes = (callog.getCallDuration() / 3600) / 60;
            int seconds = callog.getCallDuration() % 60;

            txtCallDuration.setText(String.format("%02d:%02d", minutes, seconds));

            if(callog.getCallType() == CallLog.Calls.OUTGOING_TYPE) {
                if (callog.getCallDuration() >= 10) {
                    imgCallCheck.setImageResource(R.drawable.ic_check_yes);
                }
                else{
                    imgCallCheck.setImageResource(R.drawable.ic_check_no);
                }
            }
        } catch (Exception call_adapter) {
            App.getInstance().trackException(call_adapter);
            call_adapter.printStackTrace();
        }
        return view ;
    }

    public CalLogAdapter(Context context, List<Callog> callogList) {
        super(context, R.layout.call_log_entry, callogList);
        this.context = context;
        this.callogList = callogList;
    }
}
