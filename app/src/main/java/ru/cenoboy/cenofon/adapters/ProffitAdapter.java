package ru.cenoboy.cenofon.adapters;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.FilterQueryProvider;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ru.cenoboy.cenofon.sqlite.DbOpenHelper;
import ru.cenoboy.cenofon.R;

/**
 * Created by radik on 09.09.14.
 */
public class ProffitAdapter extends CursorAdapter implements FilterQueryProvider {

    private Context context;

    public ProffitAdapter(Context context) {
        super(context, null);
        this.context = context;
        setFilterQueryProvider(this);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = (View)inflater.inflate(R.layout.proffit_item,null);
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Log.d("how_work", "ProffitAdapter.bindView()");
        TextView banner = (TextView)view.findViewById(R.id.banner);
        banner.setText(cursor.getString(cursor.getColumnIndex("name")));

        TextView moneySum = (TextView)view.findViewById(R.id.moneySum);
        moneySum.setText(cursor.getString(cursor.getColumnIndex("money")));

        TextView dateTime = (TextView)view.findViewById(R.id.dateTime);

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        SimpleDateFormat dateFormatHuman = new SimpleDateFormat(
                "dd.MM.yyyy HH:mm:ss", Locale.getDefault());

        dateTime.setText("");
        try {
            Date d = dateFormat.parse(cursor.getString(cursor.getColumnIndex("date")));
            dateTime.setText(dateFormatHuman.format(d));
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    @Override
    public Cursor runQuery(CharSequence charSequence) {
        SQLiteDatabase db = DbOpenHelper.getInstance(context).getDb();



        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        String paramDate = dateFormat.format(new Date(new Date().getTime()
                - (long) 24 * (long) 3600 * (long) 1000));

        if (charSequence != null) {
            if (charSequence.equals("day")) {
                ;
            }

            if (charSequence.equals("week")) {
                paramDate = dateFormat.format(new Date(new Date().getTime() - (long)7 * (long)24 *(long) 3600 * (long)1000));
            }

            if (charSequence.equals("month")) {
                paramDate = dateFormat.format(new Date(new Date().getTime() - (long)31 * (long)24 * (long)3600 * (long)1000));
            }


            return db.rawQuery("" +
                            "SELECT " +
                            "   1               AS '_id', " +
                            "   proffits.name   AS 'name', " +
                            "   proffits.money  AS 'money', " +
                            "   proffits.date   AS 'date' " +
                            "FROM " +
                            "   proffits " +
                            "WHERE proffits.date>=? " +
                            "UNION " +
                            "SELECT "+
                            "   1               AS '_id', " +
                            "   seen.name   AS 'name', " +
                            "   seen.money  AS 'money', " +
                            "   seen.date   AS 'date' " +
                            "FROM " +
                            "   seen " +
                            "WHERE seen.date>=? ",
                    new String[]{ paramDate, paramDate});
        }

        Cursor cur = db.rawQuery("SELECT count(*) FROM seen", null);
        cur.moveToNext();
        int q = cur.getInt(0);

        return db.rawQuery(""+
                        "SELECT " +
                        "   1               AS '_id', " +
                        "   proffits.name   AS 'name', " +
                        "   proffits.money  AS 'money', " +
                        "   proffits.date   AS 'date' " +
                        "FROM " +
                        "   proffits " +
                        "UNION " +
                        "SELECT "+
                        "   1               AS '_id', " +
                        "   seen.name   AS 'name', " +
                        "   seen.money  AS 'money', " +
                        "   seen.date   AS 'date' " +
                        "FROM " +
                        "   seen", null);
    }
}
