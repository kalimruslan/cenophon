package ru.cenoboy.cenofon.adapters;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.FilterQueryProvider;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ru.cenoboy.cenofon.sqlite.DbOpenHelper;
import ru.cenoboy.cenofon.R;

/**
 * Created by radik on 09.09.14.
 */
public class ReferalAdapter extends CursorAdapter implements FilterQueryProvider {

    private Context context;

    public ReferalAdapter(Context context) {
        super(context, null);
        this.context = context;
        setFilterQueryProvider(this);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = (View)inflater.inflate(R.layout.referal_item,null);
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Log.d("how_work", "ReferalAdapter.bindView()");
        TextView name = (TextView)view.findViewById(R.id.referalName);
        name.setText(cursor.getString(cursor.getColumnIndex("name")));


        TextView money = (TextView)view.findViewById(R.id.referalMoney);
        money.setText(cursor.getString(cursor.getColumnIndex("money")));
    }

    @Override
    public Cursor runQuery(CharSequence charSequence) {
        SQLiteDatabase db = DbOpenHelper.getInstance(context).getDb();

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        if (charSequence != null) {
            if (charSequence.equals("day")) {
                return db.rawQuery("SELECT 1 as '_id', name, sum(money) as money FROM referals WHERE date>=?  GROUP BY name", new String[]{
                        dateFormat.format(new Date(new Date().getTime() - (long)24 * (long)3600 * (long)1000))
                });
            }

            if (charSequence.equals("week")) {
                return db.rawQuery("SELECT 1 as '_id', name, sum(money) as money FROM referals WHERE date>=?  GROUP BY name", new String[]{
                        dateFormat.format(new Date(new Date().getTime() - (long)7 * (long)24 * (long)3600 * (long)1000))
                });
            }

            if (charSequence.equals("month")) {
                return db.rawQuery("SELECT 1 as '_id', name, sum(money) as money FROM referals  WHERE date>=? GROUP BY name", new String[]{
                        dateFormat.format(new Date(new Date().getTime() - (long)31 * (long)24 * (long)3600 * (long)1000))
                });
            }
        }
        return db.rawQuery("SELECT 1 as '_id', name, sum(money) as money FROM referals GROUP BY name", null);
    }

}
