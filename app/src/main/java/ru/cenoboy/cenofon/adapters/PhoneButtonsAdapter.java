package ru.cenoboy.cenofon.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.cenoboy.cenofon.R;

/**
 * Created by radik on 03.09.14.
 */
public class PhoneButtonsAdapter extends BaseAdapter {
    private static final String TAG = "PhoneButtonsAdapter" ;
    private Context mContext;

    public  PhoneButtonsAdapter(Context c) {
        mContext = c;

    }

    @Override
    public int getCount() {
        return 12;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    public String getDigit(int i) {
        JSONArray data = null;
        try {
            data = new JSONArray(mContext.getString(getStringIDByPos(i)));
            return data.getString(0);
        } catch (JSONException e) {
            Log.e(TAG, "Ошибка в проге: "+e.getMessage());
            return null;
        }
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        JSONArray data = null;
        try {
            data = new JSONArray(mContext.getString(getStringIDByPos(i)));
        } catch (JSONException e) {
            Log.e(TAG, "Ошибка в проге: "+e.getMessage());
            return null;
        }
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.button_phone, null);

            TextView textView = (TextView)view.findViewById(R.id.textView);
            TextView textViewLoc = (TextView)view.findViewById(R.id.textViewLoc);
            TextView textViewlat = (TextView)view.findViewById(R.id.textViewLat);


            try {
                textView.setText(data.getString(0));
            } catch (JSONException e) {
                Log.e(TAG, "Ошибка в проге: " + e.getMessage());
                return null;
            }
            try {
                textViewLoc.setText(getSymbolsFromArray(data.getJSONArray(1)));
            } catch (JSONException e) {
                Log.e(TAG, "Ошибка в проге: " + e.getMessage());
                return null;
            }
            try {
                textViewlat.setText(getSymbolsFromArray(data.getJSONArray(2)));
            } catch (JSONException e) {
                Log.e(TAG, "Ошибка в проге: " + e.getMessage());
                return null;
            }
        }

        return view;
    }

    private String getSymbolsFromArray(JSONArray jsonArray) {
        String res = "";
        for(int i = 0;i<jsonArray.length();i++)
            try {
                res+=jsonArray.get(i);
            } catch (JSONException e) {
                ;
            }
        return res;
    }

    /**
     * Возвращаетя идентификатор ресурса где хранится json массив описывающий содержимое кнопки
     * @param i {int}   Позиция элемента в gridview
     * @return {int}    идентификатор ресурса
     */
    public static int getStringIDByPos(int i) {
        switch (i) {
            case 0:
                return R.string.button0;
            case 1:
                return R.string.button1;
            case 2:
                return R.string.button2;
            case 3:
                return R.string.button3;
            case 4:
                return R.string.button4;
            case 5:
                return R.string.button5;
            case 6:
                return R.string.button6;
            case 7:
                return R.string.button7;
            case 8:
                return R.string.button8;
            case 9:
                return R.string.button9;
            case 10:
                return R.string.button10;
            case 11:
                return R.string.button11;
            default:
                return 0;
        }
    }



    public String getRegExpString() {
        return "";
    }

}
