package ru.cenoboy.cenofon;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseCrashReporting;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.List;
import java.util.Map;

import ru.cenoboy.cenofon.sqlite.DbOpenHelper;

//import com.crittercism.app.Crittercism;

/**
 * Created by plekhanov on 27.03.15.
 */
public class App extends Application {


    public static long calltime = 0;
    public static String cur_id = "";
    public static double cur_profit=0;
    public static String cur_name = "";
    public static long durtime = 15;
    public static long timeout = 0;

    private static String mDeviceID = "";
    private static App mInstance;

    @SuppressWarnings("finally")
    public static String getDeviceID() {
        return mDeviceID;
    }

    @Override
    public void onCreate() {
        Log.d("how_work", "APP.onCreate()");
        super.onCreate();

        mInstance = this;
        AnalyticsTrackers.initialize(this);
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);

        initDeviceID();
        //Crittercism.initialize(getApplicationContext(), "552084b58172e25e679066a5");
        ParseCrashReporting.enable(this);
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "TI9RKJ7Qhs9QLS94Hq5qYKc0GA9P9NdUHdCxNmgc", "RY9naIsExcTXl1QatNhnHgcTeJGrr14o4ZDYMM7t");
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        //installation.put("accounts",getAccounts());
        installation.saveInBackground();

        ParsePush.subscribeInBackground("", new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });

        ParsePush.subscribeInBackground("user_" + getDeviceID(), new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });

        //createContactsBook(); // своя тел.книга

        //DbOpenHelper.getInstance(this).getDb().execSQL("DELETE FROM showtimes");

        //throw new RuntimeException("Test Exception!");
    }

    public static synchronized App getInstance() {
        return mInstance;
    }

    private static final String[] PROJECTION_PHONE = {
            ContactsContract.CommonDataKinds.Phone._ID,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER
    };

    /**
     * Экспорт телефонной книжки девайса в свою таблицу SQLite
     */
    private void createContactsBook() {
        Log.d("how_work", "APP.onCreate().createContactsBook");

        Cursor cur = this.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                PROJECTION_PHONE,null, null, null);

        SQLiteDatabase db = DbOpenHelper.getInstance(this).getDb();

        int i =0;

        db.beginTransaction();
        try {
            db.execSQL("DELETE from contacts");

            while (cur.moveToNext()) {
                Log.d("how_work", "APP.createContactsBook.while");

                String number = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                db.execSQL("INSERT INTO contacts VALUES (?,?)", new String[]{number, name});
                Log.d("how_work", "Insert: " + new String[]{number, name});
            }
            cur.close();
            db.setTransactionSuccessful();
        } catch (Exception e) {
            ;
        } finally {
            db.endTransaction();
        }
    }

    private void initDeviceID() {
        Context context = getApplicationContext();
        String deviceID = "";
        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            deviceID = tm.getDeviceId();
        } catch (Exception e) {
            return;
        }

        if (deviceID == null) {
            try {
                WifiManager wifimanager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiinfo = wifimanager.getConnectionInfo();
                deviceID = wifiinfo.getMacAddress();
            } catch (Exception e) {
                return;
            }
        }

        mDeviceID = deviceID;
    }


    public static void UpdateUserInfo(ParseObject ui, ContentValues values) {

        // solution for Transmission < 11
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            for (Map.Entry<String, Object> item : values.valueSet()) {
                String key = item.getKey(); // getting key
                Object value = item.getValue(); // getting value
                ui.put(key,value.toString());
            }
        }

        // solution for Transmission >= 11
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            for(String key : values.keySet()) {
                ui.put(key,values.getAsString(key));
            }
        }
        ui.saveInBackground();
    }


    public static void UpdateUserInfo(final ContentValues values) {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("UserInfo");
        query.whereEqualTo("chanell","user_"+getDeviceID());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e!=null)
                    return;

                boolean hasResult = false;
                if (parseObjects!=null)
                    if (parseObjects.size()>0)
                        hasResult = true;

                if (hasResult) {
                    parseObjects.get(0).fetchIfNeededInBackground(new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject parseObject, ParseException e) {
                            if (e!=null)
                                return;

                            if (parseObject == null)
                                return;

                            UpdateUserInfo(parseObject,values);

                        }
                    });
                } else {
                    ParseObject ui = new ParseObject("UserInfo");
                    ui.put("chanell","user_"+getDeviceID());
                    UpdateUserInfo(ui, values);
                }

            };


        });
    }

    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */
    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();

        // Set screen name.
        t.setScreenName(screenName);

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    /***
     * Tracking exception
     *
     * @param e exception to be tracked
     */
    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                            .setDescription(
                                    new StandardExceptionParser(this, null)
                                            .getDescription(Thread.currentThread().getName(), e))
                            .setFatal(false)
                            .build()
            );
        }
    }

    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */
    public void trackEvent(String category, String action, String label) {
        Tracker t = getGoogleAnalyticsTracker();

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
    }
}
