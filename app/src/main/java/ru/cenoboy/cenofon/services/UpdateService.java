package ru.cenoboy.cenofon.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

import ru.cenoboy.cenofon.App;
import ru.cenoboy.cenofon.Constants;
import ru.cenoboy.cenofon.R;
import ru.cenoboy.cenofon.Transmission;
import ru.cenoboy.cenofon.activities.PhoneNumberActivity;
import ru.cenoboy.cenofon.asynctasks.ExecuteServiceAsyncTask;
import ru.cenoboy.cenofon.asynctasks.LoadAdvTask;
import ru.cenoboy.cenofon.sqlite.DbOpenHelper;

/**
 * Created by plekhanov on 18.03.15.
 */
public class UpdateService extends IntentService{

    String userId; //ID пользователя
    SimpleDateFormat dateFormat;

    public UpdateService() {
        super("PricephoneUpdateService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        userId = intent.getStringExtra("user_id");
        //Будем делать обмен данными через каждые 15 минут
        while (true) {
            Exchange(userId);
            try {
                Thread.sleep(15*60*1000);
                //Thread.sleep(30*60*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void Exchange(final String userId) {
        dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        //Сообщим список просмотренных видосов
        Cursor cur = DbOpenHelper.getInstance(getApplicationContext()).getDb().rawQuery("SELECT * FROM seen", null);
        String videoId;
        String dateViewInPhone;

        ArrayList<String>listVideoId = new ArrayList<String>();
        ArrayList<String>listVideoDate = new ArrayList<String>();
        while (cur.moveToNext()) {
            listVideoId.add(cur.getString(0));
            listVideoDate.add(cur.getString(1));
        }

        // Если есть видос на отправку
        if (cur.getCount()>0) {
            videoId = listVideoId.get(listVideoId.size() - 1);
            dateViewInPhone = listVideoDate.get(listVideoDate.size() - 1).toString();
            ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            params.add(new BasicNameValuePair("user_id", userId));
            params.add(new BasicNameValuePair("video_id", videoId));
            params.add(new BasicNameValuePair("date_view_in_phone", dateViewInPhone));
            new ExecuteServiceAsyncTask(getApplicationContext(),
                    Constants.COMMAND_VIDEO_VIEW_ADD, params).execute(); // Отправляем на фиксацию
        }
        else{
            try{
                ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
                params.add(new BasicNameValuePair("user_id", userId));
                new ExecuteServiceAsyncTask(getApplicationContext(),
                        Constants.COMMAND_GET_AV_VIDEO, params).execute(); // Получаем доступные видосы
            } catch (Exception e3){e3.printStackTrace();}
        }
    }
    // Ответ от сервера на фиксацию видео
    public void parseResponceVideoViewAdd (final Context context, final JSONObject jsonResult) {
        Handler h = new Handler(context.getMainLooper());
        Log.d("how_work", "UpdateService() parseResponceVideoViewAdd(): " + jsonResult);
        DbOpenHelper.getInstance(context).getDb().execSQL("DELETE FROM seen");
        if (jsonResult != null) {
            try {
                if (jsonResult.getString("status").equals("error")) {
                    if (jsonResult.getString("error").equals("wrong_user_id")) {
                        h.post(new Runnable() {
                            @Override
                            public void run() {
                             Toast.makeText(context, R.string.wrong_user_id, Toast.LENGTH_LONG).show();
                            }
                        });
                        return;
                    }
                    if (jsonResult.getString("error").equals("user_not_found")) {
                        h.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, R.string.user_not_found, Toast.LENGTH_LONG).show();
                            }
                        });
                        return;
                    }
                    if (jsonResult.getString("error").equals("wrong_video_id")) {
                        h.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, R.string.wrong_video_id, Toast.LENGTH_LONG).show();
                            }
                        });
                        return;
                    }
                    if (jsonResult.getString("error").equals("not_available")) {
                        h.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, R.string.not_available, Toast.LENGTH_LONG).show();
                            }
                        });
                        return;
                    }
                    if (jsonResult.getString("error").equals("limit")) {
                        h.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, R.string.limit, Toast.LENGTH_LONG).show();
                            }
                        });
                        return;
                    }
                }

            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
        else{
            return;
        }
    }

    // Ответ от сервера на запрос баланса
    public void parseResponceGetUserBalance (final Context context, final JSONObject jsonResult) {
        Log.d("how_work", "UpdateService() parseResponceGetUserBalance(): " + jsonResult);
        if (jsonResult != null) {
            try {
                SharedPreferences pref = context.getSharedPreferences("settings", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putFloat("balance", Float.parseFloat(String.format( "%.2f", Float.parseFloat(jsonResult.getString("balance")) / 10000)));
                Log.d("how_work", "balance: " + jsonResult.getJSONObject("params").getDouble("user_balance"));
                editor.commit();

                Intent i = new Intent();
                i.setAction("ru.cenofon.pricephone.balance");
                sendBroadcast(i);
            } catch (Exception ignored) {
                Log.d("how_work", "UpdateService.getUserBalance.Exception(): " + ignored);
            }
        }
        else{
            return;
        }
    }

    // Ответ от сервера на доступное видео
    public void parseResponceGetAvVideo(final Context context, final JSONObject jsonResult){
        Log.d("how_work", "UpdateService() parseResponceGetAvVideo(): " + jsonResult);
        Handler h = new Handler(context.getMainLooper());
        if (jsonResult != null) {
            try {
                if (jsonResult.getString("status").equals("error")) {
                    if (jsonResult.getString("error").equals("disable")) {
                        PhoneNumberActivity.limitSoonVideo = true;
                        h.post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Toast.makeText(context, jsonResult.getJSONObject("message").getString("text"), Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    if (jsonResult.getString("error").equals("limit")) {
                        PhoneNumberActivity.limitSoonVideo = true;
                        h.post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    String tt = jsonResult.getJSONObject("message").getString("text");
                                    //Toast.makeText(context, tt, Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
                else {
                    new LoadAdvTask(context, jsonResult).execute();
                    PhoneNumberActivity.limitSoonVideo = false;
                }
            } catch (Exception ignored) {
                ignored.printStackTrace();
            }
        }
        else{return;}
    }

    // Проверяем нужно ли скачивать видосы
    public static void work(Context context,JSONObject adv_result, LoadAdvTask loadAdvTask) throws JSONException {

        JSONObject video;
        String test = adv_result.get("video").toString();
        if (!test.equals("[]")) {
            video = adv_result.getJSONObject("video");
        } else {
            video = new JSONObject();
        }

        ArrayList<String>ids = new ArrayList<String>(); //тут будут загруженные идентификаторы
        ArrayList<String> idVideos = new ArrayList<String>(); //тут будут загруженные идентификаторы

        Iterator<String> keys = video.keys();

        while (keys.hasNext()){
            try {
                String id = keys.next();
                ids.add(id);

                JSONObject value = video.getJSONObject(id);
                String videoId = value.getString("id");
                idVideos.add(videoId);
                //Прежде чем загружать проверим не загружено ли уже
                if (isExistAndNormal(context,videoId))
                    continue;

                value = video.getJSONObject(id);
                download(context,videoId,value);
            } catch (Exception e) {
            }
        }

        //После загрузки почистим все видосы что есть на диске но не вошли в список
        clearOldVideos(context, idVideos);
    }

    //Пробегает по папкам и удаляет те папке что отсутствуют в списке загруженных видео
    private static void clearOldVideos(Context context, ArrayList<String> ids) {
        String videoFolder = Environment.getExternalStorageDirectory()+"/cf_video";
        String specFolder = context.getFilesDir().getAbsolutePath()+"/spec";

        File folder = new File(videoFolder);
        if (!folder.exists())
            return;

        for(String id:folder.list()) {
            if (!ids.contains(id)) {
                try {
                    PhoneNumberActivity.DeleteRecursive(new File(videoFolder+"/"+id));
                    File spec = new File(specFolder+"/"+id);
                    if (spec.exists())
                        PhoneNumberActivity.DeleteRecursive(spec);
                } catch (Exception e) {
                    ;
                }
            }

        }
    }

    /**
     * Проверяет нет ли уже папки с таким идентификатором и есть ли в ней нормальное содержимое
     * @param id идентификатор видео
     */
    private static boolean isExistAndNormal(Context context,String id) {

        String videoFolder = Environment.getExternalStorageDirectory()+"/cf_video/"+id;
        String specFolder = context.getFilesDir().getAbsolutePath()+"/spec/"+id;

        File fVideo = new File(videoFolder);
        File fSpec = new File(specFolder);
        if (!fVideo.exists() || !fSpec.exists()) {
            return false;
        }

        // если нет ничего
        if ((fVideo.list().length!=1) || (fSpec.list().length!=1)) {
            try {
                PhoneNumberActivity.DeleteRecursive(fVideo);
                PhoneNumberActivity.DeleteRecursive(fSpec);
            } catch (Exception e) {
            }
            return false;
        }


        boolean json = false;
        for(String name:fVideo.list()) {
            if ((new File(videoFolder+"/"+name)).length()==0) {
                try {
                    PhoneNumberActivity.DeleteRecursive(fVideo);
                    PhoneNumberActivity.DeleteRecursive(fSpec);
                } catch (Exception e) {
                }
                return false;
            }

            if (fSpec.list().length==1) {
                for (String spname: fSpec.list()) {
                    if (spname.equals("spec.json"))
                        json = true;
                }
            }
        }

        if (!json) {
            try {
                PhoneNumberActivity.DeleteRecursive(fVideo);
                PhoneNumberActivity.DeleteRecursive(fSpec);
            } catch (Exception e) {
            }
            return false;
        }


        return true;
    }

    // Скачиваем видосы
    private static void download(Context context,String id, JSONObject item) throws Exception {
        Log.d("how_work", "UpdateService() download start: " + id);
        //Убедимся что есть папка куда будем загружать файлы
        String videoFolder = Environment.getExternalStorageDirectory().toString()+"/cf_video/"+id;
        String specFolder = context.getFilesDir().getAbsolutePath().toString()+"/spec/"+id;

        File fVideo = new File(videoFolder);
        File fSpec = new File(specFolder);
        if (!fVideo.exists())
            fVideo.mkdirs();

        if (!fSpec.exists())
            fSpec.mkdirs();

        final String filename = videoFolder+"/video.mp4";
        //Загрузим файл
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(Transmission.host+"video/"+item.getString("file_name"));
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage());
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = connection.getContentLength();

            // download the file
            input = new BufferedInputStream( connection.getInputStream());
            output = new FileOutputStream(filename);

            byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
            }
        } catch (Exception e) {
            App.getInstance().trackException(e);
            e.printStackTrace();
            Toast.makeText(context, "Не все видео удалось скачать", Toast.LENGTH_SHORT).show();
        } finally {
            try {
                output.flush();
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }

        //Запишем спецификацию файла
        String filename_spec = specFolder+"/spec.json";
        FileOutputStream spec_os = new FileOutputStream(filename_spec);
        OutputStreamWriter writer = new OutputStreamWriter(spec_os);
        writer.write(item.toString());
        writer.flush();
        spec_os.close();

        Log.d("how_work", "UpdateService() download finish: " + id);
        return;
    }

    // Фиксация видео после совершенного звонка
    public static void setViewAfterCall(final Context context, final String userId) {

        final SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        //Сообщим список просмотренных видосов
        Cursor cur = DbOpenHelper.getInstance(context).getDb().rawQuery("SELECT * FROM seen",null);
        String videoId;
        String dateViewInPhone;

        ArrayList<String>listVideoId = new ArrayList<String>();
        ArrayList<String>listVideoDate = new ArrayList<String>();
        while (cur.moveToNext()) {
            listVideoId.add(cur.getString(0));
            listVideoDate.add(cur.getString(1));
        }
        if (cur.getCount()>0) {
            videoId = listVideoId.get(listVideoId.size() - 1);
            ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            params.add(new BasicNameValuePair("user_id", userId));
            params.add(new BasicNameValuePair("video_id", videoId));
            new ExecuteServiceAsyncTask(context, Constants.COMMAND_VIDEO_VIEW_ADD, params).execute();
        }
    }
}
