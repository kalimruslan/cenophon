package ru.cenoboy.cenofon.services;

import android.annotation.TargetApi;
import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.SyncStateContract;
import android.util.Log;

import com.google.android.gms.location.LocationServices;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import ru.cenoboy.cenofon.Constants;
import ru.cenoboy.cenofon.activities.PhoneNumberActivity;
import ru.cenoboy.cenofon.asynctasks.ExecuteServiceAsyncTask;

public class SendGeoService extends IntentService {

    private String curAddress;
    private String ipAddres;
    String userId;

    public SendGeoService() {
        super("SendGeoService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        userId = intent.getStringExtra("user_id");
        while (true) {
            sendGeoParameters(userId);
            try {
                // Через каждые 12 часов 720
                Thread.sleep(720 * 60 * 1000);

                //Через каждые  30секунд
                //Thread.sleep(30 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    protected void sendGeoParameters(final String userId){
        SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
        double Latitude = pref.getFloat("cur_lat", 0);
        double Longitude = pref.getFloat("cur_lng", 0);
        Log.d("geo_lock", "SERVICE: LAT: " + Latitude + " LNG: " + Longitude);
        if (Latitude != 0 || Longitude != 0) {
            try {
                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(Latitude, Longitude, 1);

                if (list != null) {
                    if (list.size() > 0) {
                        Address address = list.get(0);
                        curAddress = address.toString();
                    }
                }
                Log.d("geo_lock", "SERVICE: sendGeoParameters -> address: " + curAddress);
                ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
                JSONObject jsonObject = new JSONObject();
                try {
                    params.add(new BasicNameValuePair("user_id", userId));
                    jsonObject.put("geo", curAddress);
                    params.add(new BasicNameValuePair("google", jsonObject.toString()));
                    new ExecuteServiceAsyncTask(Constants.COMMAND_SEND_GEO, params).execute();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            catch (Exception ignored) {}
        }
        /*else{
            Log.d("geo", "SendGeoService - sendGeoParameters -> PhoneNumberActivity.mLastLocation: " + PhoneNumberActivity.mLastLocation + " is null");
            ipAddres = getIpAddress();
            ExecuteServiceAsyncTask ex = new ExecuteServiceAsyncTask(Constants.COMMAND_GET_SYPEX_JSON, ipAddres, userId);
            ex.execute();

        }*/
        Log.d("geo_lock", "SERVICE: sendGeoParameters FINISH");
    }

    public void parseResponceSendGeo(JSONObject jsonResult){
        Log.d("geo_lock", "SendGeoService - parseResponceSendGeo -> jsonResult: " + jsonResult);
        //TODO Добавить ответ от сервера
    }

    public void parseResponceGetSypex(JSONObject jsonResult, String userId){
        Log.d("geo", "SendGeoService - parseResponceGetSypex -> jsonResult: " + jsonResult);
        final String sypexResult = String.valueOf(jsonResult);

        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        JSONObject jsonObject = new JSONObject();
        try {
            params.add(new BasicNameValuePair("user_id", userId));
            jsonObject.put("geo", sypexResult);
            params.add(new BasicNameValuePair("sypex", jsonObject.toString()));
            new ExecuteServiceAsyncTask(Constants.COMMAND_SEND_WITHOUT_GEO, params).execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void parseResponceSendWithoutGeo(JSONObject jsonResult){
        Log.d("geo_lock", "SendGeoService - parseResponceSendWithoutGeo -> jsonResult: " + jsonResult);
        //TODO Добавить ответ от сервера
    }

    public static String getIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (SocketException e) {
            // Log.e(Constants.LOG_TAG, e.getMessage(), e);
        }
        return null;
    }
}
