package ru.cenoboy.cenofon.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ru.cenoboy.cenofon.App;
import ru.cenoboy.cenofon.R;
import ru.cenoboy.cenofon.activities.PhoneNumberActivity;
import ru.cenoboy.cenofon.services.UpdateService;
import ru.cenoboy.cenofon.sqlite.DbOpenHelper;

/**
 * Created by plekhanov on 22.04.15.
 */
public class PhoneStateReceiver extends BroadcastReceiver {

    private long limitTime;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("how_work", "PhoneStatereceiver.onReceive()");
        //We listen to two intents.  The new outgoing call only tells us of an outgoing call.  We use it to get the number.
        if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
            String savedNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");
        }
        else{
            String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
            String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            int state = 0;
            if(stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)){
                state = TelephonyManager.CALL_STATE_IDLE;

                if (((App)context.getApplicationContext()).cur_id!=null) {
                    if (!((App) context.getApplicationContext()).cur_id.equals("")) {
                        if (((App) context.getApplicationContext()).calltime != 0) {
                            if (((new Date()).getTime() - ((App) context.getApplicationContext()).calltime) > ((App) context.getApplicationContext()).durtime * 1000) {
                                if (!PhoneNumberActivity.limitSoonVideo) {
                                    Log.d("how_work", "PhoneStatereceiver.onReceive().SQLite INSERT");
                                    DbOpenHelper.getInstance(context).getDb().execSQL("INSERT INTO seen (id,date,money,name) VALUES (?,?,?,?)",
                                            new String[]{((App) context.getApplicationContext()).cur_id,
                                                    getDateTime(),
                                                    String.valueOf(((App) context.getApplicationContext()).cur_profit),
                                                    ((App) context.getApplicationContext()).cur_name});
                                }


                                SharedPreferences pref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
                                String username = pref.getString("first_name", "");
                                String userId = pref.getString("user_id","");

                                try {
                                    UpdateService.setViewAfterCall((App) context.getApplicationContext(), userId);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText((App) context.getApplicationContext(), "Баланс обновится в ближайшее время.", Toast.LENGTH_SHORT).show();
                                }
                                Log.d("how_work", "PhoneStatereceiver.UpdateService.setViewAfterCall");

                                if (!PhoneNumberActivity.limitSoonVideo) {
                                    MediaPlayer mp = MediaPlayer.create(context, R.raw.moneta);
                                    mp.start();
                                    Toast.makeText(context, username + ", ПОЗДРАВЛЯЕМ! ВАМ НАЧИСЛЕН ДОХОД " +
                                            String.valueOf(((App) context.getApplicationContext()).cur_profit) +
                                            " РУБ ЗА ПРОСМОТР РОЛИКА", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                    }
                }

                ((App)context.getApplicationContext()).cur_id = "";
                ((App)context.getApplicationContext()).cur_profit=0;
                ((App)context.getApplicationContext()).durtime = 15;
                ((App)context.getApplicationContext()).cur_name = "";
                ((App)context.getApplicationContext()).calltime = 0;

            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
                state = TelephonyManager.CALL_STATE_OFFHOOK;
            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)){
                state = TelephonyManager.CALL_STATE_RINGING;
            }


            //onCallStateChanged(context, state, number);
        }
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
}
