package ru.cenoboy.cenofon.broadcastreceivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import org.json.JSONObject;

import java.util.Date;

import ru.cenoboy.cenofon.R;
import ru.cenoboy.cenofon.activities.MessageActivity;

/**
 * Created by plekhanov on 05.04.15.
 */
public class PushReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("how_work", "PushReceiver.onReceive()");
        try {
            //String channel = intent.getStringExtra("com.parse.Channel");
            String data = intent.getStringExtra("com.parse.Data");

            JSONObject object = new JSONObject(data);

            String alert = object.getString("alert");
            if (alert.startsWith("cf_")) {
                execCommand(context,alert);
                return;
            }

            // this
            String ns = Context.NOTIFICATION_SERVICE;
            NotificationManager mNotificationManager = (NotificationManager)context.getSystemService(ns);


            int icon = R.mipmap.ic_launcher;
            CharSequence tickerText = alert; // ticker-text
            long when = System.currentTimeMillis();

            long time = new Date().getTime();
            String tmpStr = String.valueOf(time);
            String last4Str = tmpStr.substring(tmpStr.length() - 5);
            int id = Integer.valueOf(last4Str);

            Intent intent2 = new Intent(context, MessageActivity.class);
            intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent2.putExtra("id", id);
            intent2.putExtra("alert", alert);


            PendingIntent contentIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intent2, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_ONE_SHOT);

            Notification notification = new Notification(icon, tickerText, when);
            notification.setLatestEventInfo(context, "Внимание!", tickerText, contentIntent);
            Uri ringURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            notification.sound = ringURI;

            mNotificationManager.notify(id, notification);

        } catch (Exception ignored) {

        }

    }

    private void execCommand(Context context, String alert) {
        SharedPreferences pref = context.getSharedPreferences("settings",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        if (alert.startsWith("cf_version")) {
            int version = Integer.parseInt(alert.replace("cf_version ","").trim());
            editor.putInt("version",version);
            editor.commit();
            return;
        }

        if (alert.startsWith("cf_clear_data")) {
            editor.remove("token");
            editor.commit();
            return;
        }

    }
}
