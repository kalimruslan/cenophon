package ru.cenoboy.cenofon;

import android.os.Handler;
import android.util.Log;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;


/**
 * Created by plekhanov on 14.10.14.
 */
public class Transmission {
    public static String host = "http://phone2.pobedim.com/";
    //public static String host = "http://test.phone.pobedim.com/";

    public static Handler uiHandler;
    static HttpURLConnection urlConnection;
    static URL urlRequest;
    static JSONObject finalResult;

    // Работа с БД и получение ответа JSON
    public static JSONObject makeHttpPostRequest(final int tag, final String command, final ArrayList<BasicNameValuePair> params) {

        InputStream is = null;
        try {
            urlRequest = new URL(host + command + "?" + getQuery(params));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String result = "";
        try {
            urlConnection = (HttpURLConnection) urlRequest.openConnection();
            urlConnection.setRequestMethod("GET"); // set POST request? because we are send parameters
            urlConnection.setConnectTimeout(7000); // Может вернуть SocketTimeoutException при 300, что-то с Proxy связано при мобильном инете

            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Server returned HTTP " + urlConnection.getResponseCode()
                        + " " + urlConnection.getResponseMessage());
            }

            is = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            result = sb.toString();

            try {
                finalResult = new JSONObject(result);
                Log.d("how_work", "JSON-1A: " + finalResult );
            } catch (JSONException exc) {
                throw new Exception("JSONException: " + result);
            }

        } catch (final Exception e) {
            e.printStackTrace();
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return finalResult;

    }

    // Соединение с интернет для получения версии приложения
    private static JSONObject makeGetRequest(String command) {
        String result = null;
        InputStream is = null;

        try {
            urlRequest = new URL(host + command);
            urlConnection = (HttpURLConnection)urlRequest.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Server returned HTTP " + urlConnection.getResponseCode()
                        + " " + urlConnection.getResponseMessage());
            }

            is = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null){
                sb.append(line + "\n");
            }
            result = sb.toString();
            finalResult = new JSONObject(result);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(urlConnection != null)
                urlConnection.disconnect();
        }

        return finalResult;
    }

    public static JSONObject executeSipyx(final int tag,final String urlReq, final String param) {

        InputStream is = null;
        try {
            urlRequest = new URL(urlReq);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        String result = "";
        try {
            urlConnection = (HttpURLConnection) urlRequest.openConnection();
            urlConnection.setRequestMethod("GET"); // set POST request? because we are send parameters
            urlConnection.setDoInput(true); // use Get request
            urlConnection.setDoOutput(true);
            OutputStream os = urlConnection.getOutputStream(); // get output parameters
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(param); // write our pairs
            writer.flush();
            writer.close();
            os.close();

            is = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            result = sb.toString();

            try {
                finalResult = new JSONObject(result);
                Log.d("how_work", "JSON-1A(Sypex): " + finalResult );
            } catch (JSONException exc) {
                throw new Exception("JSONException: " + result);
            }

        } catch (final Exception e) {
            e.printStackTrace();
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return finalResult;
    }

    public static JSONObject executeSendGeo(final int tag, final String command, final ArrayList<BasicNameValuePair> params) {

        InputStream is = null;
        try {
            urlRequest = new URL(host + command + "?" + getQueryGeo(params));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String result = "";
        try {
            urlConnection = (HttpURLConnection) urlRequest.openConnection();
            urlConnection.setRequestMethod("GET"); // set POST request? because we are send parameters
            urlConnection.setDoInput(true); // use Get request
            urlConnection.setDoOutput(true);

            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new Exception("Server returned HTTP " + urlConnection.getResponseCode()
                        + " " + urlConnection.getResponseMessage());
            }

            is = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            result = sb.toString();

            try {
                finalResult = new JSONObject(result);
                Log.d("how_work", "JSON-1A: " + finalResult );
            } catch (JSONException exc) {
                throw new Exception("JSONException: " + result);
            }

        } catch (final Exception e) {
            e.printStackTrace();
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return finalResult;

    }

    // Формирование URL
    private static String getQuery(ArrayList<BasicNameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (BasicNameValuePair pair : params) {
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }
        Log.d("query", "getQuery: " + result.toString());
        return result.toString();
    }

    // Формирование URL
    private static String getQueryGeo(ArrayList<BasicNameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (BasicNameValuePair pair : params) {
            if (first)
                first = false;
            else
                result.append("&");
            result.append(pair.getName());
            result.append("=");
            result.append(pair.getValue());
        }
        Log.d("query", "getQuery: " + result.toString());
        return result.toString();
    }

    // Подтверждение наставника
    public static JSONObject getAppVersion() {
        return makeGetRequest("system/app/ver");
    }
    // Получение статуса пользователя
    public static JSONObject getUserStatus(int tag, ArrayList<BasicNameValuePair> params) {
        return makeHttpPostRequest(tag, "user/status", params);
    }
    // Регистрация пользователя
    public static JSONObject userRegister(int tag, ArrayList<BasicNameValuePair> params) throws JSONException {
        return makeHttpPostRequest(tag, "user/registration", params);
    }
    //Подтверждение телефона (отпрака кода из смс)
    public static JSONObject confirmPhone(int tag,  ArrayList<BasicNameValuePair> params) {
        return makeHttpPostRequest(tag, "user/phone/confirm", params);
    }
    // Подтверждение наставника
    public static JSONObject confirmMentor(int tag, ArrayList<BasicNameValuePair> params) {
        return makeHttpPostRequest(tag, "user/mentor/confirm", params);
    }
    // Получение информации о пользователе (получение баланса)
    public static JSONObject getUserBalance(int tag, ArrayList<BasicNameValuePair> params) throws JSONException {
        return makeHttpPostRequest(tag, "user/get/info", params);
    }
    //TODO Сделать когда будет готово
    public static void getUserPeriodProfit(int tag, String user_id) throws JSONException {
        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair("user_id", user_id));
        JSONArray paramsJson = new JSONArray();
        paramsJson.put("user_period_profit");

        params.add(new BasicNameValuePair("params", paramsJson.toString()));
        makeHttpPostRequest(tag, "user/get/info", params);
    }
    //TODO Сделать когда будет готово
    public static void getUserPeriodProfitReferal(int tag, String user_id) throws JSONException {
        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
        params.add(new BasicNameValuePair("user_id", user_id));
        JSONArray paramsJson = new JSONArray();
        paramsJson.put("user_period_profit_referral");

        params.add(new BasicNameValuePair("params", paramsJson.toString()));
        makeHttpPostRequest(tag, "user/get/info", params);
    }

    // Получение доступных видеоматериалов
    public static JSONObject getAvVideo(int tag, ArrayList<BasicNameValuePair> params) {
        return makeHttpPostRequest(tag, "video/get/available", params);
    }

    // Зафиксировать просмотр видео
    public static JSONObject videoViewAdd(int tag, ArrayList<BasicNameValuePair> params){
        return makeHttpPostRequest(tag, "video/view/add", params);
    }

    public static JSONObject sendGeo(int tag, ArrayList<BasicNameValuePair> params){
        return executeSendGeo(tag, "user/set/geo", params);
    }

    public static JSONObject getSypexJson(int tag, String ipAddress){
        return executeSipyx(tag, "http://api.sypexgeo.net/json/", ipAddress);
    }

    public static JSONObject sendWithoutGeo(int tag, ArrayList<BasicNameValuePair> params){
        return makeHttpPostRequest(tag, "user/set/geo", params);
    }

    // Вывод средств на ценоденьги
    public static JSONObject withDraw(int tag, ArrayList<BasicNameValuePair> params){
       return makeHttpPostRequest(tag, "user/withdraw", params);
    }


}
