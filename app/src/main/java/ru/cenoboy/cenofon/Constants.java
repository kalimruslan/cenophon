package ru.cenoboy.cenofon;

/**
 * Created by User on 14.12.2015.
 */
public class Constants {
    public final static int COMMAND_GET_USER_STATUS = 0; // Команда для получения статуса пользователя
    public final static int COMMAND_USER_REGISTER = 1; // Команда на регистрацию пользователя
    public final static int COMMAND_CONFIRM_PHONE = 2; // Команда на подтверждение телефона
    public final static int COMMAND_CONFIRM_MENTOR = 3; // Команда на подтверждение наставника
    public final static int COMMAND_GET_USER_BALANCE = 4; // Команда на получение баланса пользователя
    public final static int COMMAND_GET_USER_PERIOD_PROFFIT = 5;
    public final static int COMMAND_GET_USER_PERIOD_PROFFIT_REFERAL = 6;
    public final static int COMMAND_GET_AV_VIDEO = 7; // Команда на получение доступных видеоматериалов
    public final static int COMMAND_VIDEO_VIEW_ADD = 8; //Команда н афиксацию видео
    public final static int COMMAND_WITHDRAW = 9; //Команда на вывод средств
    public final static int COMMAND_SEND_GEO = 10; //Команда на вывод средств
    public final static int COMMAND_SEND_WITHOUT_GEO = 11;
    public final static int COMMAND_GET_SYPEX_JSON = 12;

    public static final int COMMAND_GET_APP_VERSION = 13;
}
