package ru.cenoboy.cenofon.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import ru.cenoboy.cenofon.R;
import ru.cenoboy.cenofon.adapters.ReferalAdapter;
import ru.cenoboy.cenofon.interfaces.IUpdateBalance;

/**
 * Created by radik on 08.09.14.
 */
public class ReferalsFragment extends Fragment implements IUpdateBalance {
    private View V;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("how_work", "ConfirmActivity.onCreate()");
        // Inflate the layout for this fragment
        V = inflater.inflate(R.layout.tab_referals, container, false);

        ListView listView = (ListView)V.findViewById(R.id.listViewRederals);
        final ReferalAdapter adapter = new ReferalAdapter(getActivity());
        listView.setAdapter(adapter);
        adapter.getFilter().filter("day");

        Spinner spinner = (Spinner)V.findViewById(R.id.peroids);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapterSpiner = ArrayAdapter.createFromResource(getActivity(),
                R.array.periods_data, R.layout.period_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapterSpiner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapterSpiner);

        //UpdateBalance();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        adapter.getFilter().filter("day");
                        break;
                    case 1:
                        adapter.getFilter().filter("week");
                        break;
                    case 2:
                        adapter.getFilter().filter("month");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ((Button)V.findViewById(R.id.moneyout)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               new ProffitFragment().onMoneyOut(getActivity(), ReferalsFragment.this);
            }
        });

        return V;
    }

    @Override
    public void UpdateBalance() {

        /*SharedPreferences pref = getActivity().getSharedPreferences("settings", Context.MODE_PRIVATE);
        float balance = pref.getFloat("balance", 0);

        ((TextView)V.findViewById(R.id.textViewBalance)).setText(String.valueOf(balance));*/
    }
}
