package ru.cenoboy.cenofon.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.cenoboy.cenofon.Constants;
import ru.cenoboy.cenofon.R;
import ru.cenoboy.cenofon.activities.PhoneNumberActivity;
import ru.cenoboy.cenofon.adapters.ProffitAdapter;
import ru.cenoboy.cenofon.asynctasks.ExecuteAsyncTask;
import ru.cenoboy.cenofon.interfaces.IUpdateBalance;

/**
 * Created by radik on 08.09.14.
 */
public class ProffitFragment extends Fragment implements IUpdateBalance {

    private boolean withdrawToPhone = false;

    private View V;
    private static Handler handler;
    SharedPreferences pref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
        Log.d("how_work", "ProffitFragment.onCreateView()");
        // Inflate the layout for this fragment
        V = inflater.inflate(R.layout.tab_proffit, container, false);

        handler = new Handler();

        ListView listView = (ListView)V.findViewById(R.id.listViewProffit);
        ProffitAdapter adapter = new ProffitAdapter(getActivity());
        listView.setAdapter(adapter);
        adapter.getFilter().filter(null);

        Spinner spinner = (Spinner)V.findViewById(R.id.peroids);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapterSpiner = ArrayAdapter.createFromResource(getActivity(),
                                                      R.array.periods_data, R.layout.period_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapterSpiner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapterSpiner);

        UpdateBalance();

        ((Button)V.findViewById(R.id.moneyout)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                withdrawToPhone = false;
                onMoneyOut(getActivity(), ProffitFragment.this);
            }
        });

        // Вывод на мобильный телефон
        ((Button)V.findViewById(R.id.moneyout_to_pone)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                withdrawToPhone = true;
                onMoneyOut(getActivity(), ProffitFragment.this);
            }
        });

        return V;
    }

    public  void onMoneyOut(final Context context, final Fragment fragment) {
        Log.d("how_work", "ProffitFragment.onMoneyOut().ENTER");
        pref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        final String userId = pref.getString("user_id", "");
        float balance = pref.getFloat("balance", 0);
        String userName = pref.getString("first_name", "");

        int minimum = 5;

        if (balance<minimum) {

            new AlertDialog.Builder(context).setTitle("Обратите внимание")
                    .setMessage("Минимальная сумма для вывода Ваших доходов "+String.valueOf(minimum)+" руб.")
                    .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    }).create().show();
        } else {
            Log.d("how_work", "ProffitFragment.onMoneyOut().ELSE");
            if(!withdrawToPhone) {
                new AlertDialog.Builder(context).setTitle("Вывести доходы")
                        .setMessage(userName + ", доходы будут выведены на Ваш баланс системы ЦеноДеньги. Для дальнейшего управления войдите в свой личный кабинет на Cenoboy.com")
                        .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setPositiveButton("Вывести сейчас", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                Thread t = new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
                                        params.add(new BasicNameValuePair("user_id", userId));
                                        params.add(new BasicNameValuePair("charge_phone", "false"));
                                        new ExecuteAsyncTask(ProffitFragment.this, Constants.COMMAND_WITHDRAW, params).execute();
                                    }
                                });

                                t.start();


                            }
                        }).create().show();
            }
            else if(withdrawToPhone){
                new AlertDialog.Builder(context).setTitle("Вывести доходы")
                        .setMessage(userName + ", доходы будут выведены на Ваш мобильный телефон.")
                        .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setPositiveButton("Вывести сейчас", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                Thread t = new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
                                        params.add(new BasicNameValuePair("user_id", userId));
                                        params.add(new BasicNameValuePair("charge_phone", "true"));
                                        new ExecuteAsyncTask(ProffitFragment.this, Constants.COMMAND_WITHDRAW, params).execute();
                                    }
                                });

                                t.start();


                            }
                        }).create().show();
            }
        }
    }

    public void parseResponseWithDraw(final JSONObject jsonResult){
        if (jsonResult != null) {
            try {
                if (jsonResult.getString("status").equals("error")) {
                    if (jsonResult.getString("error").equals("disable")) {
                        Log.d("how_work", "ProffitFragment.onMoneyOut().DISABLE");
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Toast.makeText(getActivity(), jsonResult.getJSONObject("message").getString("text"), Toast.LENGTH_LONG).show();
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        });
                        return;
                    }
                    if (jsonResult.getString("error").equals("wrong_user_id")) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), R.string.wrong_user_id, Toast.LENGTH_LONG).show();
                            }
                        });
                        return;
                    }
                    if (jsonResult.getString("error").equals("user_not_found")) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), R.string.user_not_found, Toast.LENGTH_LONG).show();
                            }
                        });
                        return;
                    }
                } else {
                    Log.d("how_work", "ProffitFragment.onMoneyOut().SUCCESS");

                    SharedPreferences.Editor edit = pref.edit();
                    edit.putFloat("balance", 0);
                    edit.commit();

                    if (handler != null) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                UpdateBalance();
                                Toast.makeText(getActivity(), "Поздравляем! Ваши доходы успешно выведены.", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                }
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void UpdateBalance() {
        float balance = PhoneNumberActivity.getBalance(getActivity().getApplicationContext());
        Log.d("how_work", "ProffitFragment.UpdateBalance() : " + balance);

        ((TextView)V.findViewById(R.id.textViewBalance)).setText(String.format("%.2f", balance));
    }


}
